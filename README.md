# Admin Dashboard

An admin dashboard for hair salon franchises.

# Demo (Hosted on Firebase)

[Admin Dashboard Client](https://hair-salon-admin.web.app/)

## Built With

[React](https://www.reactjs.org/)

## Credential

-   Franchise Owner/who also manage central inventory

```
username: superadmin
password: 12345
```

-   Franchisee store 1 manager

```
username: store1
password: 12345
```

-   Franchisee store 2 manager

```
username: store2
password: 12345
```

## Authors

-   **Westley Tan** - Contributor

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
