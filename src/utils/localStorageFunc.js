export function getUser() {
    const payload = localStorage.getItem('user')
    if (typeof payload === 'string') return JSON.parse(payload)
    else return payload
}

export function getCheckId() {
    const payload = localStorage.getItem('getCheckId')
    if (typeof payload === 'string') return JSON.parse(payload)
    else return payload
}

export function storeAnItem(item, payload) {
    if (typeof payload !== 'string') localStorage.setItem(item, JSON.stringify(payload))
    else localStorage.setItem(item, payload)
}
