import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Table, Button, message } from 'antd'
import '@/style/view-style/table.scss'
import axios from '@/api'
import { API } from '@/api/config'
import { altImageSource } from '../../../api/config'
import { getCheckId } from '../../../utils/localStorageFunc'

class MerchandiseList extends Component {
    state = {
        entryList: []
    }

    addButton = () =>
        this.props.history.push({
            from: this.props.location.pathname,
            pathname: '/merchandise/addMerchandise'
        })

    editMerchandise = text =>
        this.props.history.push({
            from: this.props.location.pathname,
            pathname: '/merchandise/EditMerchandise',
            search: '?_id=' + text._id,
            state: text
        })

    deleteMerchandise = _id => {
        if (!window.confirm('Please confirm the delete operation!')) return

        const original = this.state.entryList
        this.setState({ entryList: original.filter(m => m._id !== _id) })
        axios
            .delete(`${API}/Merchandise/deleteMerchandise/${_id}`)
            .then(({ status }) => status === 200 && message.success('Delete Success!'))
            .catch(() => {
                message.error('Delete Failed!')
                this.setState({ entryList: original })
            })
    }

    componentDidMount = () => this.fetchMerchandiseList()

    fetchMerchandiseList = () =>
        axios
            .get(`${API}/merchandise/getAllMerchandise?adminid=${getCheckId().adminId}`)
            .then(({ status, data }) => status === 200 && this.setState({ entryList: data.data }))
            .catch(() => message.error('Error On Retrieving Data.'))

    render() {
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Merchandise List']} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Preview',
                                        dataIndex: 'imgURL',
                                        key: 'imgURL',
                                        render: a =>
                                            a && a.length > 0 ? (
                                                <div className='central__merchandise__image--container'>
                                                    <img
                                                        src={API + a[a.length - 1].slice(6, 35)}
                                                        onError={e => (e.target.src = altImageSource)}
                                                        alt={a[0]}
                                                        className='central__merchandise__image--thumbnail'
                                                    />
                                                </div>
                                            ) : (
                                                'No images'
                                            )
                                    },

                                    {
                                        title: 'Name',
                                        dataIndex: 'name',
                                        key: 'name'
                                    },
                                    {
                                        title: 'Category',
                                        dataIndex: 'categoryId.name',
                                        key: 'category'
                                    },
                                    {
                                        title: 'Price',
                                        dataIndex: 'price',
                                        key: 'price'
                                    },
                                    {
                                        title: 'Stock',
                                        dataIndex: 'stock',
                                        key: 'stock'
                                    },
                                    {
                                        title: 'Minimal Stock',
                                        dataIndex: 'minStock',
                                        key: 'minStock'
                                    },
                                    {
                                        title: 'Note',
                                        dataIndex: 'note',
                                        key: 'note'
                                    },
                                    {
                                        title: 'Action',
                                        key: 'action',
                                        render: (text, record) => (
                                            <span>
                                                <Button type='link' onClick={() => this.editMerchandise(text)}>
                                                    Edit
                                                </Button>

                                                <Button type='link' onClick={() => this.deleteMerchandise(text._id)}>
                                                    Delete
                                                </Button>
                                            </span>
                                        )
                                    }
                                ]}
                                dataSource={this.state.entryList}
                            />
                        </div>
                    </Col>
                </Row>
                <div className='float-right'>
                    <Button style={{}} type='primary' htmlType='submit' onClick={this.addButton}>
                        Add Merchandise
                    </Button>
                </div>
            </Layout>
        )
    }
}

export default MerchandiseList
