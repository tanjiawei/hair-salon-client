import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Table, message } from 'antd'
import '@/style/view-style/table.scss'
import axios from '@/api'
import { API } from '@/api/config'
import { getCheckId } from '../../../utils/localStorageFunc'

class ShortageList extends Component {
    state = {
        entryList: []
    }

    componentDidMount = () => {
        axios
            .get(`${API}/merchandise/getAllMerchandise?adminid=${getCheckId().adminId}`)
            .then(res => {
                if (res.status === 200) {
                    let data = []
                    console.log(res.data)
                    res.data.data.forEach(entry => {
                        if (entry.stock < entry.minStock)
                            data.push({
                                _id: entry._id,
                                name: entry.name,
                                stock: entry.stock,
                                minStock: entry.minStock
                            })
                    })
                    this.setState({ entryList: data })
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    render() {
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Shortage List']} />
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Name',
                                        dataIndex: 'name',
                                        key: 'name'
                                    },
                                    {
                                        title: 'Stock',
                                        dataIndex: 'stock',
                                        key: 'stock'
                                    },
                                    {
                                        title: 'Minimum Stock',
                                        dataIndex: 'minStock',
                                        key: 'minStock'
                                    }
                                ]}
                                dataSource={this.state.entryList}
                            />
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

export default ShortageList
