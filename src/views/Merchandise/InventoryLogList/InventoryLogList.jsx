import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Table, message, Button, Pagination } from 'antd'
import '@/style/view-style/table.scss'
import axios from '@/api'
import { API } from '@/api/config'
import moment from 'moment'
import { getCheckId } from '../../../utils/localStorageFunc'
class InventoryLogList extends Component {
    state = {
        logData: [],
        totalCount: 0
    }

    componentDidMount = () => {
        this.fetchData()
    }

    fetchData = (pageNumber = 1, totalSize = 10) => {
        axios
            .get(
                `${API}/inventoryLog/getAllInventoryLog?id=${
                    getCheckId().adminId
                }&pageNumber=${pageNumber}&pageSize=${totalSize}`
            )
            .then(({ status, data: { totalCount, logList } }) => {
                if (status === 200) {
                    const logData = logList.map(l => ({
                        _id: l._id,
                        merchandiseName: l.merchandiseDoc[0].name,
                        stockLog: l.stockLog,
                        createdAt: l.createdAt ? moment(l.createdAt).format('MMM DD, YYYY HH:mm') : '',
                        orderId: l.orderId,
                        type: l.type
                    }))
                    this.setState({ logData, totalCount })
                }
            })
            .catch(message.log)
    }

    addButton = () =>
        this.props.history.push({
            from: this.props.location.pathname,
            pathname: '/merchandise/AddInventoryLog'
        })

    render() {
        const { logData, totalCount } = this.state
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Inventory Log List']} />
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Merchandise Name',
                                        dataIndex: 'merchandiseName',
                                        key: 'merchandiseName'
                                    },
                                    {
                                        title: 'Stock Change',
                                        dataIndex: 'stockLog',
                                        key: 'stockLog'
                                    },
                                    {
                                        title: 'Order Id',
                                        dataIndex: 'orderId',
                                        key: 'orderId'
                                    },
                                    {
                                        title: 'Type',
                                        dataIndex: 'type',
                                        key: 'type'
                                    },
                                    {
                                        title: 'Created Time',
                                        dataIndex: 'createdAt',
                                        key: 'createdAt'
                                    }
                                ]}
                                dataSource={logData}
                                pagination={{ hideOnSinglePage: true }}
                            />
                            <Pagination
                                onChange={this.fetchData}
                                total={totalCount}
                                className='d-flex justify-content-end mt-4'
                            />
                        </div>
                    </Col>
                </Row>
                <div style={{ float: 'right' }}>
                    <Button style={{}} type='primary' htmlType='submit' onClick={this.addButton}>
                        Record Inventory Change
                    </Button>
                </div>
            </Layout>
        )
    }
}

export default InventoryLogList
