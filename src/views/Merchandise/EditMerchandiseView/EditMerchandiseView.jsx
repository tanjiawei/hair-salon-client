import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, Select, message, Checkbox } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'
import { getCheckId } from '../../../utils/localStorageFunc'
import AppImagesList from '../../../components/renderImages/AppImagesList'

class EditMerchandiseView extends Component {
    constructor(props) {
        super(props)
        // React Anti-pattern
        const defaultProp = props.location.state

        this.state = {
            _id: defaultProp._id,
            name: defaultProp.name,
            price: defaultProp.price,
            stock: defaultProp.stock,
            minStock: defaultProp.minStock,
            note: defaultProp.note,
            imgURL: defaultProp.imgURL,
            categoryId: defaultProp.categoryId._id,
            categoryList: [],
            isForService: defaultProp.isForService,
            imgFiles: []
        }
    }

    componentDidMount = () => this.fetchCategoryList()

    fetchCategoryList = () =>
        axios
            .get(`${API}/merchandiseCategory/getAllCategory?id=${getCheckId().adminId}`)
            .then(({ status, data }) => status === 200 && this.setState({ categoryList: data }))
            .catch(() => message.error('Error On Retrieving Data.'))

    handleImageChange = e => {
        const imgFiles = [...this.state.imgFiles]
        const FileList = e.target.files
        Object.keys(FileList).map(file => (file !== 'length' ? imgFiles.push(FileList[file]) : null))
        this.setState({ imgFiles })
    }

    handleDelete = source => i => {
        if (!window.confirm('Confirmed to remove the image?')) return
        const newSource = [...this.state[source]]
        newSource.splice(i, 1)
        this.setState({ [source]: newSource })
    }

    handleSubmit = e => {
        e.preventDefault()
        const { imgFiles, imgURL } = this.state
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return
            const { name, categoryId, price, stock, minStock, note, isForService } = fieldsValue

            const data = {
                name,
                categoryId,
                price,
                stock,
                minStock,
                note,
                isForService,
                imgURL: JSON.stringify(imgURL)
            }
            const postData = new FormData()

            Object.keys(data).forEach(entry => postData.append(entry, data[entry]))

            imgFiles.map(img => postData.append('img', img))
            imgFiles.length = 0

            axios
                .put(`${API}/Merchandise/updateMerchandise/${this.state._id}`, postData)
                .then(({ status }) => status === 200 && (window.location = this.props.location.from))
                .catch(() => message.error('Edit Failed!'))
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const {
            imgFiles,
            imgURL,
            name,
            price,
            stock,
            minStock,
            note,
            categoryId,
            categoryList,
            isForService
        } = this.state

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Service Management', 'Edit Merchandise']} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label={<span>Name</span>}>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please input name' }],
                                        initialValue: name
                                    })(<Input placeholder='Please input name' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label='Category'>
                                    {getFieldDecorator('categoryId', {
                                        rules: [{ required: true, message: 'Please choose a category' }],
                                        initialValue: categoryId
                                    })(
                                        <Select
                                            name='categoryId'
                                            id='categoryId'
                                            placeholder='Please choose a category'>
                                            {categoryList.map(entry => (
                                                <option key={entry._id} value={entry._id}>
                                                    {entry.name}
                                                </option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>

                                <Form.Item label={<span>Price</span>}>
                                    {getFieldDecorator('price', {
                                        rules: [{ required: true, message: 'Please input price' }],
                                        initialValue: price
                                    })(<Input type='number' min={0} step={0.01} placeholder='Please input price' />)}
                                </Form.Item>

                                <Form.Item label={<span>Stock</span>}>
                                    {getFieldDecorator('stock', {
                                        rules: [{ required: true, message: 'Please input stock' }],
                                        initialValue: stock
                                    })(<Input type='number' min={0} step={1} placeholder='Please input stock' />)}
                                </Form.Item>

                                <Form.Item label={<span>Minimal Stock</span>}>
                                    {getFieldDecorator('minStock', {
                                        initialValue: minStock ? minStock : 0
                                    })(
                                        <Input
                                            type='number'
                                            min={0}
                                            step={1}
                                            placeholder='Please input minimum stock'
                                        />
                                    )}
                                </Form.Item>

                                <Form.Item label={<span>Note</span>}>
                                    {getFieldDecorator('note', {
                                        initialValue: `${note}`
                                    })(<Input.TextArea rows={4} autoSize placeholder='Please input note' />)}
                                </Form.Item>

                                <Form.Item label={<span>For Service</span>}>
                                    {getFieldDecorator('isForService', {
                                        initialValue: isForService
                                    })(<Checkbox defaultChecked={isForService} />)}
                                </Form.Item>

                                <Form.Item label={<span>Picture</span>}>
                                    <Button>
                                        <input type='file' multiple onChange={this.handleImageChange} />
                                    </Button>
                                </Form.Item>
                                <Col>
                                    <div className='base-style'>
                                        {imgFiles[0] && (
                                            <AppImagesList
                                                srcFormat={item => URL.createObjectURL(item)}
                                                source={imgFiles}
                                                onClickContainer={this.handleDelete('imgFiles')}
                                                title='Pending to be uploaded'
                                            />
                                        )}
                                        {imgURL[0] && (
                                            <AppImagesList
                                                srcFormat={item => API + item.slice(6, 35)}
                                                source={imgURL}
                                                onClickContainer={this.handleDelete('imgURL')}
                                                title='Pictures'
                                            />
                                        )}
                                    </div>
                                </Col>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit' className='mr-5'>
                                        Save
                                    </Button>
                                    <Button type='secondary' onClick={this.props.history.goBack}>
                                        Go Back
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(EditMerchandiseView)

export default WrappedNormalLoginForm
