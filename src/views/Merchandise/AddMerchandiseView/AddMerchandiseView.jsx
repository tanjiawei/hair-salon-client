import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, Select, message, Checkbox } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'
import { getCheckId } from '../../../utils/localStorageFunc'
import AppImagesList from '../../../components/renderImages/AppImagesList'

class AddMerchandiseView extends Component {
    state = {
        categoryList: [],
        imgFiles: []
    }

    componentDidMount = () => this.fetchCategoryList()

    fetchCategoryList = () =>
        axios
            .get(`${API}/merchandiseCategory/getAllCategory?id=${getCheckId().adminId}`)
            .then(({ status, data }) => status === 200 && this.setState({ categoryList: data }))
            .catch(() => message.error('Error On Retrieving Data.'))

    handleImageChange = e => {
        const imgFiles = [...this.state.imgFiles]
        const FileList = e.target.files
        Object.keys(FileList).map(file => (file !== 'length' ? imgFiles.push(FileList[file]) : null))
        this.setState({ imgFiles })
    }

    handleDelete = source => i => {
        if (!window.confirm('Confirmed to remove the image?')) return
        const newSource = [...this.state[source]]
        newSource.splice(i, 1)
        this.setState({ [source]: newSource })
    }

    handleSubmit = e => {
        e.preventDefault()
        const { imgFiles } = this.state
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return

            const { name, categoryId, price, stock, minStock, note, isForService } = fieldsValue

            const data = {
                name,
                categoryId,
                price,
                stock,
                minStock: minStock ?? 0,
                note: note ?? '',
                isForService: isForService ?? false,
                adminid: getCheckId().adminId
            }
            const postData = new FormData()

            Object.keys(data).forEach(entry => {
                postData.append(entry, data[entry])
            })

            imgFiles.map(img => postData.append('img', img))
            imgFiles.length = 0

            axios
                .post(`${API}/Merchandise/addMerchandise`, postData)
                .then(({ status }) => status === 200 && (window.location = this.props.location.from))
                .catch(() => message.error('An error appeared, please try again!'))
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const { categoryList, imgFiles } = this.state

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Service Management', 'Add Merchandise']} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label={<span>Name</span>}>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please input name' }]
                                    })(<Input placeholder='Please input name' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label='Category'>
                                    {getFieldDecorator('categoryId', {
                                        rules: [{ required: true }],
                                        initialValue: ''
                                    })(
                                        <Select
                                            name='categoryId'
                                            id='categoryId'
                                            placeholder='Please choose a category'>
                                            {categoryList.map(entry => (
                                                <option key={entry._id} value={entry._id}>
                                                    {entry.name}
                                                </option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>

                                <Form.Item label={<span>Price</span>}>
                                    {getFieldDecorator('price', {
                                        rules: [{ required: true, message: 'Please input price' }]
                                    })(<Input placeholder='Please input price' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Stock</span>}>
                                    {getFieldDecorator('stock', {
                                        rules: [{ required: true, message: 'Please input stock' }]
                                    })(<Input placeholder='Please input stock' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Minimal Stock</span>}>
                                    {getFieldDecorator('minStock')(
                                        <Input placeholder='Please input minimal' value={'fsdfs'} />
                                    )}
                                </Form.Item>

                                <Form.Item label={<span>Note</span>}>
                                    {getFieldDecorator('note')(
                                        <Input.TextArea rows={4} autoSize placeholder='Please input note' />
                                    )}
                                </Form.Item>

                                <Form.Item label={<span>For Service</span>}>
                                    {getFieldDecorator('isForService')(<Checkbox></Checkbox>)}
                                </Form.Item>

                                <Form.Item label={<span>Picture</span>}>
                                    <Button>
                                        <input type='file' multiple onChange={this.handleImageChange} />
                                    </Button>
                                </Form.Item>
                                <Col>
                                    <div className='base-style'>
                                        {imgFiles[0] && (
                                            <AppImagesList
                                                srcFormat={item => URL.createObjectURL(item)}
                                                source={imgFiles}
                                                onClickContainer={this.handleDelete('imgFiles')}
                                                title='Pending to be uploaded'
                                            />
                                        )}
                                    </div>
                                </Col>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit' className='mr-5'>
                                        Add
                                    </Button>
                                    <Button type='secondary' onClick={this.props.history.goBack}>
                                        Go Back
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(AddMerchandiseView)

export default WrappedNormalLoginForm
