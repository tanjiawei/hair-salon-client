import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, Select, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'
import { getCheckId } from '../../../utils/localStorageFunc'

class AddInventoryView extends Component {
    state = {
        confirmDirty: false,
        AutoCompleteResult: [],
        visible: true,
        merchandiseList: []
    }

    componentDidMount = () => {
        this.fetchMerchandiseList()
    }

    fetchMerchandiseList = () => {
        axios
            .get(`${API}/merchandise/getAllMerchandise?adminid=${getCheckId().adminId}`)
            .then(res => {
                if (res.status === 200) {
                    console.log(res.data)
                    let data = []
                    res.data.data.forEach(entry => {
                        data.push({
                            _id: entry._id,
                            name: entry.name,
                            stock: entry.stock,
                            minStock: entry.minStock
                        })
                    })
                    this.setState({ merchandiseList: data })
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return

            let { merchandiseId, stockLog } = fieldsValue

            let postData = {
                merchandiseId: merchandiseId,
                adminId: getCheckId().adminId,
                stockLog: stockLog
            }

            axios
                .post(`${API}/inventoryLog/addInventoryLog`, postData)
                .then(res => {
                    if (res.status === 200) {
                        window.location = this.props.location.from
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    console.log(err)
                })
        })
    }

    handleConfirmBlur = e => {
        const { value } = e.target
        this.setState({ confirmDirty: this.state.confirmDirty || !value })
    }

    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Add Log']} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label='Merchandise Name'>
                                    {getFieldDecorator('merchandiseId', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please choose a Merchandise'
                                            }
                                        ]
                                    })(
                                        <Select
                                            name='merchandiseId'
                                            id='merchandiseId'
                                            placeholder='Please choose a Merchandise'>
                                            {this.state.merchandiseList.map(entry => (
                                                <option value={entry._id}>{entry.name}</option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>

                                <Form.Item label={<span>Stock Change</span>}>
                                    {getFieldDecorator('stockLog', {
                                        rules: [{ required: true, message: 'Please input stock change' }]
                                    })(<Input placeholder='Please input stock change' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit' className='mr-5'>
                                        Add
                                    </Button>
                                    <Button type='secondary' onClick={this.props.history.goBack}>
                                        Go Back
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(AddInventoryView)

export default WrappedNormalLoginForm
