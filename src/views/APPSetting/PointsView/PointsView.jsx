import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Icon, Input, Tooltip, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

class FromView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            visible: true,
            share_value: 0,
            service_value: 0,
            complete_value: 0,
            openAccount_value: 0,
            type1_get: '',
            type1_pay: '',
            type2_get: '',
            type2_pay: '',
            type3_get: '',
            type3_pay: ''
        }
        props.onReSetCheckAdmin()
    }

    componentDidMount() {
        axios
            .get(`${API}/admin/getPointsSetting`)
            .then(res => {
                if (res.data.data.length === 1) {
                    for (let i = 0; i < res.data.data[0].content.length; i++) {
                        let content = res.data.data[0].content[i]
                        if (i === 0) {
                            this.setState({
                                share_value: content.value
                            })
                        } else if (i === 1) {
                            this.setState({
                                service_value: content.value
                            })
                        } else if (i === 2) {
                            this.setState({
                                complete_value: content.value
                            })
                        } else if (i === 3) {
                            this.setState({ openAccount_value: content.value })
                        }
                    }
                }
                console.log(res)
            })
            .catch(err => {
                console.log(err)
                message.error(err)
            })
    }

    handleClose = () => {
        this.setState({ visible: false })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return
            const values = {
                ...fieldsValue,
                'date-picker': fieldsValue['date-picker'] ? fieldsValue['date-picker'].format('YYYY-MM-DD') : ''
            }

            var postData = {
                share_value: values.share_value,
                service_value: values.service_value,
                complete_value: values.complete_value,
                openAccount_value: values.openAccount_value
            }
            axios
                .post(`${API}/admin/updatePointsSetting`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Success!')
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    message.error(err.response.data)
                })
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['APP Setting', 'Points']}></CustomBreadcrumb>
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label='Share moment points:'>
                                    {getFieldDecorator('share_value', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input points!'
                                            }
                                        ],
                                        initialValue: `${this.state.share_value}`
                                    })(<Input placeholder='Please input points!' />)}
                                </Form.Item>

                                <Form.Item
                                    label={
                                        <span>
                                            Purchase Service 1 point&nbsp;
                                            <Tooltip title='How much does the custmoer earn 1 point when purchasing the service'>
                                                <Icon type='question-circle-o' />
                                            </Tooltip>
                                        </span>
                                    }>
                                    {getFieldDecorator('service_value', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input a number!'
                                            }
                                        ],
                                        initialValue: `${this.state.service_value}`
                                    })(<Input placeholder='Please input a number!' />)}
                                </Form.Item>

                                <Form.Item
                                    label={
                                        <span>
                                            Complete Information
                                            <Tooltip title='The users can get points when they complete theire proifles!'>
                                                <Icon type='question-circle-o' />
                                            </Tooltip>
                                        </span>
                                    }>
                                    {getFieldDecorator('complete_value', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input a number!'
                                            }
                                        ],
                                        initialValue: `${this.state.complete_value}`
                                    })(<Input placeholder='Please input a number!' />)}
                                </Form.Item>

                                <Form.Item
                                    label={
                                        <span>
                                            Open Account
                                            <Tooltip title='The users can get points when they create an account!'>
                                                <Icon type='question-circle-o' />
                                            </Tooltip>
                                        </span>
                                    }>
                                    {getFieldDecorator('openAccount_value', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input a number!'
                                            }
                                        ],
                                        initialValue: `${this.state.openAccount_value}`
                                    })(<Input placeholder='Please input a number!' />)}
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Save
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(FromView)

export default WrappedNormalLoginForm
