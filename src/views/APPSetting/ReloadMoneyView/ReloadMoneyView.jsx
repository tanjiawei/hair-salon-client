import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

class FromView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            visible: true,
            type1_get: '',
            type1_pay: '',
            type2_get: '',
            type2_pay: '',
            type3_get: '',
            type3_pay: ''
        }
        props.onReSetCheckAdmin()
    }

    componentDidMount() {
        axios
            .get(`${API}/admin/getReloadMoneySetting`)
            .then(res => {
                if (res.data.data.length === 1) {
                    for (let i = 0; i < res.data.data[0].content.length; i++) {
                        let content = res.data.data[0].content[i]
                        if (i === 0) {
                            this.setState({
                                type1_get: content.get,
                                type1_pay: content.pay
                            })
                        } else if (i === 1) {
                            this.setState({
                                type2_get: content.get,
                                type2_pay: content.pay
                            })
                        } else if (i === 2) {
                            this.setState({
                                type3_get: content.get,
                                type3_pay: content.pay
                            })
                        }
                    }
                }
                console.log(res)
            })
            .catch(err => {
                console.log(err)
                message.error(err)
            })
    }

    handleClose = () => {
        this.setState({ visible: false })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return
            const values = {
                ...fieldsValue,
                'date-picker': fieldsValue['date-picker'] ? fieldsValue['date-picker'].format('YYYY-MM-DD') : ''
            }

            var postData = {
                type1_get: values.type1_get,
                type1_pay: values.type1_pay,
                type2_get: values.type2_get,
                type2_pay: values.type2_pay,
                type3_get: values.type3_get,
                type3_pay: values.type3_pay
            }
            axios
                .post(`${API}/admin/updateReloadMoneySetting`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Success!')
                    } else {
                        console.log(res)
                        // 这里处理一些错误信息
                    }
                })
                .catch(err => {
                    message.error(err.response.data)
                })
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Shop', 'Add Shop']}></CustomBreadcrumb>
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label='Slot 1'>
                                    {getFieldDecorator('type1_pay', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input will pay amount!'
                                            }
                                        ],
                                        initialValue: `${this.state.type1_pay}`
                                    })(<Input placeholder='Please input will pay amount!' />)}
                                    {getFieldDecorator('type1_get', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input will get amount!'
                                            }
                                        ],
                                        initialValue: `${this.state.type1_get}`
                                    })(<Input placeholder='Please input will get amount!' />)}
                                </Form.Item>
                                <Form.Item label='Slot 2'>
                                    {getFieldDecorator('type2_pay', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input will pay amount!'
                                            }
                                        ],
                                        initialValue: `${this.state.type2_pay}`
                                    })(<Input placeholder='Please input will pay amount!' />)}
                                    {getFieldDecorator('type2_get', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input will get amount!'
                                            }
                                        ],
                                        initialValue: `${this.state.type2_get}`
                                    })(<Input placeholder='Please input will get amount!' />)}
                                </Form.Item>
                                <Form.Item label='Slot 3'>
                                    {getFieldDecorator('type3_pay', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input will pay amount!'
                                            }
                                        ],
                                        initialValue: `${this.state.type3_pay}`
                                    })(<Input placeholder='Please input will pay amount!' />)}
                                    {getFieldDecorator('type3_get', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input will get amount!'
                                            }
                                        ],
                                        initialValue: `${this.state.type3_get}`
                                    })(<Input placeholder='Please input will get amount!' />)}
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Save
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(FromView)

export default WrappedNormalLoginForm
