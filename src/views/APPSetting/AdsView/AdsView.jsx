import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

import AppImagesList from '../../../components/renderImages/AppImagesList'

class FromView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            visible: true,
            imgFiles: ['', ''],
            imgurl1: '',
            imgurl2: '',
            type1_get: '',
            type1_pay: '',
            type2_get: '',
            type2_pay: '',
            type3_get: '',
            type3_pay: ''
        }
        props.onReSetCheckAdmin()
    }

    componentDidMount() {
        axios
            .get(`${API}/admin/getAdsSetting`)
            .then(res => {
                let tmp = JSON.parse(JSON.stringify(this.state.imgFiles))
                for (let i = 0; i < res.data.data[0].content.length; i++) {
                    tmp[i] = res.data.data[0].content[i]
                }
                this.setState({ imgurl1: tmp[0], imgurl2: tmp[1] })
            })
            .catch(err => {
                console.log(err)
                message.error(err)
            })
    }

    handleClose = () => {
        this.setState({ visible: false })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return

            for (let i = 0; i < this.state.imgFiles.length; i++) {
                if (this.state.imgFiles[i] === '') {
                    return message.error('Please select Slot ' + (i + 1) + ' picture')
                }
            }

            const postData = new FormData()
            // postData.append('slot', this.state.imgFiles)
            this.state.imgFiles.map(img => postData.append('img', img))

            axios
                .post(`${API}/admin/updateAdsSetting/`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Success!')
                        window.location.reload()
                    }
                })
                .catch(err => {
                    console.log(err)
                })
        })
    }

    validateToNextPassword = (rule, value, callback) => {
        const { form } = this.props
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true })
        }
        callback()
    }

    handleImageChange1 = e => {
        let tmp = this.state.imgFiles
        tmp[0] = e.target.files[0]
        this.setState({ imgFiles: tmp })
    }
    handleImageChange2 = e => {
        let tmp = this.state.imgFiles
        tmp[1] = e.target.files[0]
        this.setState({ imgFiles: tmp })
    }
    handleDelete = source => i => {
        if (!window.confirm('Confirmed to remove the image?')) return
        const newSource = [...this.state[source]]
        newSource.splice(i, 1)
        this.setState({ [source]: newSource })
    }

    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Shop', 'Add Shop']}></CustomBreadcrumb>
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label='Slot 1'>
                                    {this.state.imgurl1 !== '' ? (
                                        <AppImagesList
                                            srcFormat={item => API + item.slice(6, 35)}
                                            source={[this.state.imgurl1]}
                                            // title='Pictures'
                                            contained='false'
                                        />
                                    ) : null}
                                    {this.state.imgFiles[0] !== '' ? (
                                        <AppImagesList
                                            srcFormat={item => URL.createObjectURL(item)}
                                            source={[this.state.imgFiles[0]]}
                                            onClickContainer={this.handleDelete('imgFiles')}
                                            title='Pending to be uploaded'
                                        />
                                    ) : null}

                                    <Button>
                                        <input type='file' multiple onChange={this.handleImageChange1} />
                                    </Button>
                                </Form.Item>
                                <Form.Item label='Slot 2'>
                                    {this.state.imgurl2 !== '' ? (
                                        <AppImagesList
                                            srcFormat={item => API + item.slice(6, 35)}
                                            source={[this.state.imgurl2]}
                                            title='Pictures'
                                            contained='false'
                                        />
                                    ) : null}
                                    {this.state.imgFiles[1] !== '' ? (
                                        <AppImagesList
                                            srcFormat={item => URL.createObjectURL(item)}
                                            source={[this.state.imgFiles[1]]}
                                            onClickContainer={this.handleDelete('imgFiles')}
                                            title='Pending to be uploaded'
                                        />
                                    ) : null}
                                    <Button>
                                        <input type='file' multiple onChange={this.handleImageChange2} />
                                    </Button>
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Save
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(FromView)

export default WrappedNormalLoginForm
