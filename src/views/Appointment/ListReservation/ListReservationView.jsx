import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import moment from 'moment'
import {
    Layout,
    Row,
    Col,
    Divider,
    Button,
    Input,
    Table,
    Modal,
    // >>>>>>> 2baf5ebe4f553ae647e5fda557eb1d7c2253b5d1
    message
} from 'antd'

import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

let recordSelected
let eventItemSelected

class ListReservationView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataEvents: [],
            showConfirm: false,
            inputMessage: ''
        }
    }

    componentDidMount() {
        this.setReservationsData()
    }

    setReservationsData = () => {
        let data = []
        let eventDetail = []
        let createTime
        axios
            .post(`${API}/appointment/getAllReservation`, {})
            .then(res => {
                if (res.status === 200) {
                    if (res.data.length > 0) {
                        for (let i = 0; i < res.data.length; i++) {
                            createTime = new Date(res.data[i].createTime)
                            eventDetail = []
                            for (let j = 0; j < res.data[i].eventDetail.length; j++) {
                                eventDetail.push({
                                    _id: res.data[i].eventDetail[j]._id,
                                    day: res.data[i].eventDetail[j].day,
                                    duration: res.data[i].eventDetail[j].duration,
                                    employeeId: res.data[i].eventDetail[j].employeeId,
                                    end: res.data[i].eventDetail[j].end,
                                    selectedEmployee: res.data[i].eventDetail[j].selectedEmployee,
                                    selectedService: res.data[i].eventDetail[j].selectedService,
                                    serviceId: res.data[i].eventDetail[j].serviceId,
                                    shopAddress: res.data[i].eventDetail[j].shopAddress,
                                    shopId: res.data[i].eventDetail[j].shopId,
                                    start: res.data[i].eventDetail[j].start,
                                    status: res.data[i].eventDetail[j].status,
                                    userId: res.data[i].eventDetail[j].userId,
                                    username: res.data[i].eventDetail[j].username,
                                    cancelStatus: res.data[i].eventDetail[j].status === 'cancelled' ? true : false,
                                    cancelText:
                                        res.data[i].eventDetail[j].status === 'cancelled' ? 'Cancelled' : 'Cancel',
                                    cancelMessage: res.data[i].eventDetail[j].statusMessage
                                })
                            }
                            if (res.data[i].type !== 'shop') {
                                data.push({
                                    _id: res.data[i]._id,
                                    orderId: res.data[i].orderId,
                                    type: res.data[i].type,
                                    events: eventDetail,
                                    shopId: res.data[i].eventDetail.shopId,
                                    userId: res.data[i].eventDetail.userId,
                                    userInfo: res.data[i].userInfo,
                                    createTime: moment(createTime).format('MMM DD, YYYY HH:mm'),
                                    name: res.data[i].userInfo.name,
                                    tel: res.data[i].userInfo.tel,
                                    email: res.data[i].userInfo.email
                                })
                            } else {
                                data.push({
                                    _id: res.data[i]._id,
                                    orderId: res.data[i].orderId,
                                    type: res.data[i].type,
                                    events: eventDetail,
                                    shopId: res.data[i].eventDetail.shopId,
                                    userId: res.data[i].eventDetail.userId,
                                    userInfo: [],
                                    createTime: moment(createTime).format('MMM DD, YYYY HH:mm'),
                                    name: '',
                                    tel: '',
                                    email: ''
                                })
                            }
                        }
                        this.setState({
                            dataEvents: data
                        })
                    }
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    showModal = (record, eventItem) => {
        recordSelected = record
        eventItemSelected = eventItem
        this.setState({ showConfirm: true })
    }

    handleOk = e => {
        if (this.state.inputMessage === '') {
            message.error('Please enter a reason')
            return
        }
        let data = [...this.state.dataEvents]

        let postData = {
            _id: eventItemSelected._id,
            status: 'cancelled',
            statusMessage: this.state.inputMessage,
            orderId: recordSelected.orderId,
            eventId: eventItemSelected._id,
            userId: eventItemSelected.userId,
            shopId: eventItemSelected.shopId,
            employeeId: eventItemSelected.employeeId,
            type: 'employee',
            message:
                'You have an appointment cancelled \nname: ' +
                eventItemSelected.username +
                '\ndate: ' +
                moment(eventItemSelected.day).format('YYYY-MM-DD') +
                '\nTime: ' +
                moment(eventItemSelected.start).format('HH:mm') +
                '~' +
                moment(eventItemSelected.end).format('HH:mm'),
            flag: 'unread'
        }
        axios
            .post(`${API}/appointment/markEventStatus`, JSON.stringify(postData))
            .then(res => {
                if (res.status === 200) {
                    for (let i = 0; i < data.length; i++) {
                        if (recordSelected._id === data[i]._id) {
                            for (let j = 0; j < data[i].events.length; j++) {
                                if (eventItemSelected._id === data[i].events[j]._id) {
                                    data[i].events[j].cancelStatus = true
                                    data[i].events[j].cancelText = 'Cancelled'
                                    data[i].events[j].cancelMessage = this.state.inputMessage
                                }
                            }
                            this.setState({
                                dataEvents: data
                            })
                        }
                    }
                    recordSelected = undefined
                    eventItemSelected = undefined
                    this.setState({ showConfirm: false, inputMessage: '' })
                    message.success('Cancel Success!')
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err.message)
            })
    }

    handleCancel = e => {
        this.setState({ showConfirm: false, inputMessage: '' })
    }

    onChangeCancelMessage = e => {
        this.setState({ inputMessage: e.target.value })
    }

    render() {
        let columnsEvent = [
            {
                title: 'Order Id',
                dataIndex: 'orderId',
                key: 'orderId'
            },
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name'
            },
            {
                title: 'Tel',
                dataIndex: 'tel',
                key: 'tel'
            },
            {
                title: 'Email',
                dataIndex: 'email',
                key: 'email'
            },
            {
                title: 'Create Date',
                dataIndex: 'createTime',
                key: 'createTime',
                defaultSortOrder: 'descend',
                sorter: (a, b) => moment(a.createTime).format('x') - moment(b.createTime).format('x')
            }
        ]

        return (
            <div>
                <Layout className='animated fadeIn'>
                    <div>
                        <CustomBreadcrumb arr={['Appointment', 'Reservation List']}></CustomBreadcrumb>
                    </div>
                    <div className='base-style'>
                        <Row>
                            <Col span={24}>
                                <Table
                                    columns={columnsEvent}
                                    expandedRowRender={record =>
                                        record.events.map((eventItem, index) => (
                                            <div className='base-style' key={eventItem._id}>
                                                {eventItem.username}
                                                <Divider />
                                                {eventItem.shopAddress}
                                                <br />
                                                <br />
                                                {moment(eventItem.start).format('YYYY-MM-DD hh:mm A')}
                                                <br />
                                                <br />
                                                {eventItem.selectedService.name}
                                                <br />
                                                <br />
                                                {eventItem.selectedEmployee.name}
                                                <br />
                                                <br />
                                                {eventItem.message}
                                                <br />
                                                <br />
                                                <div style={{ textAlign: 'right' }}>
                                                    <Button
                                                        type='danger'
                                                        disabled={eventItem.cancelStatus}
                                                        onClick={() => this.showModal(record, eventItem)}>
                                                        {eventItem.cancelText}
                                                    </Button>
                                                    <p>{eventItem.cancelMessage}</p>
                                                    <Modal
                                                        title='Are you sure want to cancel?'
                                                        visible={this.state.showConfirm}
                                                        onOk={this.handleOk}
                                                        onCancel={this.handleCancel}>
                                                        <Input
                                                            value={this.state.inputMessage}
                                                            onChange={this.onChangeCancelMessage}
                                                            placeholder='Please enter a reason'
                                                        />
                                                    </Modal>
                                                </div>
                                            </div>
                                        ))
                                    }
                                    dataSource={this.state.dataEvents}
                                    rowKey='_id'
                                    pagination={{ hideOnSinglePage: true }}
                                />
                            </Col>
                        </Row>
                    </div>
                </Layout>
            </div>
        )
    }
}

export default ListReservationView
