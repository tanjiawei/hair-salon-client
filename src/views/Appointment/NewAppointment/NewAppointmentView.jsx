import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, Select, DatePicker, Tabs, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'
import moment from 'moment'
import { getCheckId } from '../../../utils/localStorageFunc'

const { Option } = Select
const { TabPane } = Tabs
const dateFormat = 'YYYY/MM/DD'

let selectedEmployeeId
let selectedServiceId
let selectedScheduleDate
let selectedEmployeeDateEvent = []
let timeTable = []
let selectedPickTime

let selectedServiceId_Quick
let selectedEmployee_Quick
let selectedDate_Quick = new Date()

class FromView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            employee: [],
            service: [],
            employeeScheduleDate: [],
            pickTime: [],
            pickEmployee: [],
            serviceSelected_Quick: '',
            employeeSelected_Quick: '',
            employeeSelected: '',
            selectedDate: '',
            selectedPickATime: '',
            serviceSelected: ''
        }
    }

    componentDidMount() {
        this.getServiceList()
    }

    getEmployeeList = () => {
        let employeeList = []

        axios
            .get(`${API}/user/getEmployeeByService/?serviceid=${selectedServiceId}`)
            .then(res => {
                if (res.status === 200) {
                    for (let i = 0; i < res.data.length; i++) {
                        for (let j = 0; j < res.data[i].employee.length; j++) {
                            employeeList.push({
                                employeeId: res.data[i].employee[j]._id,
                                name: res.data[i].employee[j].name,
                                email: res.data[i].employee[j].email,
                                phone: res.data[i].employee[j].phone
                            })
                        }
                    }
                    this.setState({
                        employee: employeeList
                    })
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    getServiceList = () => {
        let serviceList = []
        this.setState({ employee: [] })
        let adminId = getCheckId().adminId
        axios
            .get(`${API}/user/getAllServicesByAdminId?id=${adminId}`)
            .then(res => {
                if (res.status === 200) {
                    if (res.data.length > 0) {
                        for (let i = 0; i < res.data.length; i++) {
                            serviceList.push({
                                serviceId: res.data[i]._id,
                                name: res.data[i].name,
                                duration: res.data[i].duration,
                                price: res.data[i].price,
                                descripton: res.data[i].description,
                                adminid: res.data[i].adminid,
                                categoryId: res.data[i].categoryId,
                                CategoryDoc: res.data[i].CategoryDoc,
                                RoomDoc: res.data[i].RoomDoc
                            })
                        }
                        this.setState({
                            service: serviceList
                        })
                    }
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    getEmployeeScheduleById = () => {
        let empScheduleDate = []
        let scheduleDate
        let scheduleStartTime
        let scheduleEndTime
        axios
            .get(`${API}/user/getEmployeeScheduleById/${selectedEmployeeId}`)
            .then(res => {
                if (res.status === 200) {
                    if (res.data.length > 0) {
                        for (let i = 0; i < res.data.length; i++) {
                            scheduleDate = new Date(res.data[i].day)
                            scheduleStartTime = new Date(res.data[i].startTime)
                            scheduleEndTime = new Date(res.data[i].endTime)
                            empScheduleDate.push({
                                scheduleId: res.data[i]._id,
                                textDate:
                                    scheduleDate.getFullYear() +
                                    '-' +
                                    ('0' + (parseInt(scheduleDate.getMonth()) + 1)).slice(-2) +
                                    '-' +
                                    ('0' + scheduleDate.getDate()).slice(-2) +
                                    ' (' +
                                    ('0' + scheduleStartTime.getHours()).slice(-2) +
                                    ':' +
                                    ('0' + scheduleStartTime.getMinutes()).slice(-2) +
                                    '~' +
                                    ('0' + scheduleEndTime.getHours()).slice(-2) +
                                    ':' +
                                    ('0' + scheduleEndTime.getMinutes()).slice(-2) +
                                    ')',
                                day: res.data[i].day
                            })
                        }
                        this.setState({
                            employeeScheduleDate: empScheduleDate
                        })
                    }
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            let startDate
            let duration
            let { username, notes } = fieldsValue
            let event = []
            let payload
            let adminId = getCheckId().adminId
            let endDate
            let dataDay
            let checkData = false
            let employeeIdx
            let serviceIdx
            let empScheduleIdx

            if (username === undefined || username === '') {
                message.error('Please enter user name')
                return
            }

            checkData = false
            for (let i = 0; i < this.state.employee.length; i++) {
                if (selectedEmployeeId === this.state.employee[i].employeeId) {
                    checkData = true
                    employeeIdx = i
                    break
                }
            }
            if (checkData === false) {
                message.error('Invalid employee, please select again')
                return
            }

            checkData = false
            for (let i = 0; i < this.state.employeeScheduleDate.length; i++) {
                if (selectedScheduleDate === this.state.employeeScheduleDate[i].scheduleId) {
                    empScheduleIdx = i
                    checkData = true
                    break
                }
            }
            if (checkData === false) {
                message.error('Invalid date, please select again')
                return
            }

            checkData = false
            for (let i = 0; i < this.state.pickTime.length; i++) {
                if (selectedPickTime === this.state.pickTime[i].pickTimeId) {
                    checkData = true
                    break
                }
            }
            if (checkData === false) {
                message.error('Invalid pick a time, please select again')
                return
            }

            for (let i = 0; i < this.state.service.length; i++) {
                if (this.state.service[i].serviceId === selectedServiceId) {
                    duration = this.state.service[i].duration
                    serviceIdx = 1
                }
            }
            dataDay = this.state.employeeScheduleDate[empScheduleIdx].day
            startDate = new Date(this.state.pickTime[selectedPickTime].timeStamp)
            endDate = new Date(moment(startDate).add(30, 'minute'))

            event.push({
                username: username,
                userId: adminId,
                shopId: adminId,
                shopAddress: '',
                message: notes,
                employeeId: selectedEmployeeId,
                selectedEmployee: {
                    _id: selectedEmployeeId,
                    name: this.state.employee[employeeIdx].name,
                    email: this.state.employee[employeeIdx].email,
                    phone: this.state.employee[employeeIdx].phone
                },
                serviceId: selectedServiceId,
                selectedService: {
                    _id: selectedServiceId,
                    name: this.state.service[serviceIdx].name,
                    duration: this.state.service[serviceIdx].duration,
                    price: this.state.service[serviceIdx].price
                },
                duration: duration,
                day: dataDay,
                start: startDate,
                end: endDate,
                createAt: new Date()
            })

            payload = {
                shopId: adminId,
                events: event,
                type: 'shop'
            }

            axios
                .post(`${API}/user/makeReservation`, payload)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Add Success!')
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    console.log(err)
                })
        })
    }

    handleSubmitQuick = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            let { username_Quick, notes_Quick } = fieldsValue
            let event = []
            let payload
            let adminId = getCheckId().adminId
            let checkData = false
            let employeeIdx
            let serviceIdx

            if (username_Quick === undefined || username_Quick === '') {
                message.error('Please enter user name')
                return
            }

            checkData = false
            for (let i = 0; i < this.state.pickEmployee.length; i++) {
                if (selectedEmployee_Quick === this.state.pickEmployee[i].pickTimeId) {
                    checkData = true
                    employeeIdx = i
                    break
                }
            }
            if (checkData === false) {
                message.error('Invalid employee, please select again')
                return
            }
            for (let i = 0; i < this.state.service.length; i++) {
                if (this.state.service[i].serviceId === selectedServiceId_Quick) {
                    serviceIdx = 1
                }
            }
            event.push({
                username: username_Quick,
                shopId: adminId,
                shopAddress: '',
                message: notes_Quick,
                employeeId: this.state.pickEmployee[employeeIdx].employeeId,
                selectedEmployee: {
                    _id: this.state.pickEmployee[employeeIdx].employeeId,
                    name: this.state.pickEmployee[employeeIdx].employeeName,
                    email: this.state.pickEmployee[employeeIdx].employeeEmail,
                    phone: this.state.pickEmployee[employeeIdx].employeePhone
                },
                serviceId: this.state.service[serviceIdx].serviceId,
                selectedService: {
                    _id: this.state.service[serviceIdx].serviceId,
                    name: this.state.service[serviceIdx].name,
                    duration: this.state.service[serviceIdx].duration,
                    price: this.state.service[serviceIdx].price
                },
                duration: this.state.service[serviceIdx].duration,
                day: this.state.pickEmployee[employeeIdx].employeeScheduleDay,
                start: this.state.pickEmployee[employeeIdx].start,
                end: this.state.pickEmployee[employeeIdx].end,
                createAt: new Date()
            })

            payload = {
                shopId: adminId,
                events: event,
                type: 'shop'
            }

            axios
                .post(`${API}/user/makeReservation`, payload)
                .then(res => {
                    if (res.status === 200) {
                        if (res.data.error) {
                            message.error(res.data.error)
                        } else {
                            message.success('Add Success!')
                        }
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    console.log(err)
                })
        })
    }

    onEmployeeChange = value => {
        selectedEmployeeId = value
        this.setState({ employeeSelected: value })
        this.setState({ selectedDate: null })
        this.setState({ selectedPickATime: null })
        this.getEmployeeScheduleById()
    }

    onServiceChange = value => {
        selectedServiceId = value
        this.setState({ serviceSelected: value })
        this.setState({ employeeSelected: null })
        this.setState({ selectedDate: null })
        this.setState({ selectedPickATime: null })
        this.getEmployeeList()
    }

    onServiceChangeQuick = value => {
        selectedServiceId_Quick = value
        this.setState({ serviceSelected_Quick: value })
        this.setState({ employeeSelected_Quick: null })
        this.getAllEmployeeDateEvent()
    }

    getAllEmployeeDateEvent = () => {
        if (!selectedServiceId_Quick) return

        let allEmployeeDateEvent = []
        let serviceIdx

        axios
            .post(`${API}/appointment/getEmployeeByServiceAndTime`, {
                serviceId: selectedServiceId_Quick,
                searchDate: selectedDate_Quick
            })
            .then(res => {
                if (res.status === 200) {
                    for (let i = 0; i < res.data.length; i++) {
                        allEmployeeDateEvent.push(res.data[i])
                    }
                } else {
                    console.log(res)
                }

                let end = new Date()
                let currentTime = new Date()
                let timeTable = []
                let duration
                let idx = 0
                let nowTime = new Date()
                let tempDate = new Date()

                if (nowTime.getMinutes() > 0 && nowTime.getMinutes() < 15) nowTime.setMinutes(15)
                else if (nowTime.getMinutes() > 15 && nowTime.getMinutes() < 30) nowTime.setMinutes(30)
                else if (nowTime.getMinutes() > 30 && nowTime.getMinutes() < 45) nowTime.setMinutes(45)
                else if (nowTime.getMinutes() > 45) nowTime.setMinutes(60)
                for (let i = 0; i < this.state.service.length; i++) {
                    if (this.state.service[i].serviceId === selectedServiceId_Quick) {
                        duration = this.state.service[i].duration
                        break
                    }
                }
                for (let i = 0; i < this.state.service.length; i++) {
                    if (this.state.service[i].serviceId === selectedServiceId_Quick) {
                        serviceIdx = i
                    }
                }
                for (let i = 0; i < allEmployeeDateEvent.length; i++) {
                    if (moment(new Date()).format(dateFormat) !== moment(selectedDate_Quick).format(dateFormat)) {
                        nowTime = new Date(selectedDate_Quick)
                        nowTime.setHours(new Date(allEmployeeDateEvent[i].schedules.startTime).getHours(), 0, 0, 0)
                    }
                    currentTime = nowTime

                    end = new Date(selectedDate_Quick)
                    tempDate = new Date(allEmployeeDateEvent[i].schedules.endTime)
                    end.setHours(
                        tempDate.getHours(),
                        tempDate.getMinutes(),
                        tempDate.getSeconds(),
                        tempDate.getMilliseconds()
                    )
                    while (currentTime.getTime() < end.getTime()) {
                        if (!this.whetherCross(currentTime, duration, allEmployeeDateEvent[i].scheduleDoc)) {
                            timeTable.push({
                                employeeId: allEmployeeDateEvent[i].employeeId,
                                employeeName: allEmployeeDateEvent[i].employee[0].name,
                                employeeEmail: allEmployeeDateEvent[i].employee[0].email,
                                employeePhone: allEmployeeDateEvent[i].employee[0].phone,
                                employeeScheduleDay: allEmployeeDateEvent[i].schedules.day,
                                serviceId: allEmployeeDateEvent[i].serviceId,
                                serviceName: this.state.service[serviceIdx].name,
                                timeStamp: currentTime,
                                timeText:
                                    allEmployeeDateEvent[i].employee[0].name +
                                    ' (' +
                                    moment(currentTime).format('HH:mm') +
                                    '~' +
                                    moment(currentTime)
                                        .add(duration, 'minutes')
                                        .format('HH:mm') +
                                    ')',
                                timeSort: currentTime.getTime(),
                                pickTimeId: idx,
                                start: currentTime,
                                end: moment(currentTime).add(duration, 'minutes')
                            })
                            idx += 1
                        }
                        currentTime = new Date(currentTime.getTime() + 900000)
                    }
                }
                timeTable = timeTable.sort((a, b) => a.timeSort - b.timeSort)
                this.setState({
                    pickEmployee: timeTable
                })
                if (timeTable.length > 0) {
                    selectedEmployee_Quick = timeTable[0].pickTimeId
                    this.setState({ employeeSelected_Quick: timeTable[0].pickTimeId })
                }
            })
            .catch(err => {
                console.log(err)
            })
    }

    onEmployeeChange_Quick = value => {
        selectedEmployee_Quick = value
        this.setState({ employeeSelected_Quick: value })
    }

    onScheduleChange = value => {
        selectedScheduleDate = value
        this.setState({ selectedDate: value })
        this.setState({ selectedPickATime: null })
        this.getEmployeeDateEvent()
    }

    onDateChanged = (date, dateString) => {
        selectedDate_Quick = date
        selectedServiceId_Quick = this.state.serviceSelected_Quick
        this.setState({ employeeSelected_Quick: null })
        this.getAllEmployeeDateEvent()
    }

    setPickTime = () => {
        let start, end
        let currentTime
        let scheduleDateString
        let scheduleTimeString
        let scheduleTimeStart, scheduleTimeEnd
        let duration
        let idx

        for (let i = 0; i < this.state.service.length; i++) {
            if (this.state.service[i].serviceId === selectedServiceId) {
                duration = this.state.service[i].duration
            }
        }
        for (let i = 0; i < this.state.employeeScheduleDate.length; i++) {
            if (this.state.employeeScheduleDate[i].scheduleId === selectedScheduleDate) {
                scheduleDateString = this.state.employeeScheduleDate[i].textDate
                scheduleTimeString = scheduleDateString.split(' ')[1]
                scheduleTimeString = scheduleTimeString.substring(1, scheduleTimeString.length - 1)
                scheduleTimeStart = scheduleTimeString.split('~')[0]
                scheduleTimeEnd = scheduleTimeString.split('~')[1]
                scheduleDateString = scheduleDateString.split(' ')[0]
            }
        }

        start = new Date(moment(scheduleDateString + ' ' + scheduleTimeStart))
        end = new Date(moment(scheduleDateString + ' ' + scheduleTimeEnd))
        currentTime = new Date(start)
        timeTable = []
        idx = 0
        while (currentTime.getTime() < end.getTime()) {
            if (!this.whetherCross(currentTime, duration, selectedEmployeeDateEvent)) {
                timeTable.push({
                    timeStamp: currentTime,
                    pickTimeId: idx
                })
                idx += 1
            }
            currentTime = new Date(currentTime.getTime() + 900000)
        }
        this.setState({
            pickTime: timeTable
        })
    }

    onPickTimeChange = value => {
        selectedPickTime = value
        this.setState({ selectedPickATime: value })
    }

    whetherCross = (curTime, duration, events) => {
        let event_start
        let event_end
        let isCross = false
        let curStart = parseInt(curTime.getTime() / 1000)
        let curEnd = curStart + parseInt(duration) * 60

        if (events.length < 1) isCross = false
        for (let i = 0; i < events.length; i++) {
            event_start = parseInt(new Date(events[i].start).getTime() / 1000)
            event_end = parseInt(new Date(events[i].end).getTime() / 1000)

            if (curEnd <= event_start + 1 || curStart + 1 >= event_end) {
            } else {
                isCross = true
                break
            }
        }
        return isCross
    }

    getEmployeeDateEvent = () => {
        let scheduleDateString
        let dayFrom, dayTo

        selectedEmployeeDateEvent = []
        for (let i = 0; i < this.state.employeeScheduleDate.length; i++) {
            if (this.state.employeeScheduleDate[i].scheduleId === selectedScheduleDate) {
                scheduleDateString = this.state.employeeScheduleDate[i].textDate
                scheduleDateString = scheduleDateString.split(' ')[0]
            }
        }
        dayFrom = scheduleDateString
        dayTo = moment(scheduleDateString)
            .add(1, 'days')
            .format('YYYY-MM-DD')

        axios
            .post(`${API}/event/getEmployeeDateEvent`, {
                employeeId: selectedEmployeeId,
                dayFrom: dayFrom,
                dayTo: dayTo
            })
            .then(res => {
                if (res.status === 200) {
                    for (let i = 0; i < res.data.length; i++) {
                        selectedEmployeeDateEvent.push(res.data[i])
                    }
                    this.setPickTime()
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                console.log(err)
            })
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const layout = {
            labelCol: {
                span: 6
            },
            wrapperCol: {
                span: 8
            }
        }
        const tailLayout = {
            wrapperCol: {
                offset: 6,
                span: 8
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Appointment', 'New Appointment']}></CustomBreadcrumb>
                </div>
                <Row>
                    <Col span={24}>
                        <div className='base-style'>
                            <Tabs defaultActiveKey='0'>
                                <TabPane tab='Available' key='0'>
                                    <Form {...layout} name='quick' onSubmit={this.handleSubmitQuick}>
                                        <Form.Item name='searchDate' label='Date'>
                                            <DatePicker
                                                defaultValue={moment(new Date(), dateFormat)}
                                                format={dateFormat}
                                                allowClear={false}
                                                onChange={this.onDateChanged}
                                            />
                                        </Form.Item>
                                        <Form.Item name='service' label='Service' rules={[{ required: true }]}>
                                            <Select
                                                value={this.state.serviceSelected_Quick}
                                                onChange={this.onServiceChangeQuick}
                                                placeholder='Select service'>
                                                {this.state.service.map((data, index) => {
                                                    return (
                                                        <Option value={data.serviceId} key={data.serviceId}>
                                                            {data.name}
                                                        </Option>
                                                    )
                                                })}
                                            </Select>
                                        </Form.Item>
                                        <Form.Item name='employee' label='Employee' rules={[{ required: true }]}>
                                            <Select
                                                value={this.state.employeeSelected_Quick}
                                                onChange={this.onEmployeeChange_Quick}
                                                placeholder='Select employee'>
                                                {this.state.pickEmployee.map((data, index) => {
                                                    return (
                                                        <Option value={data.pickTimeId} key={data.pickTimeId}>
                                                            {data.timeText}
                                                        </Option>
                                                    )
                                                })}
                                            </Select>
                                        </Form.Item>
                                        <Form.Item label={<span>User Name</span>}>
                                            {getFieldDecorator(
                                                'username_Quick',
                                                {}
                                            )(<Input placeholder='Please input user name' />)}
                                        </Form.Item>
                                        <Form.Item label={<span>Notes</span>}>
                                            {getFieldDecorator(
                                                'notes_Quick',
                                                {}
                                            )(<Input placeholder='Please input notes' />)}
                                        </Form.Item>
                                        <Form.Item {...tailLayout}>
                                            <Button type='primary' htmlType='submit'>
                                                Add
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </TabPane>
                                <TabPane tab='Booking' key='1'>
                                    <Form {...layout} name='basic' onSubmit={this.handleSubmit}>
                                        <Form.Item label={<span>User Name</span>}>
                                            {getFieldDecorator(
                                                'username',
                                                {}
                                            )(<Input placeholder='Please input user name' />)}
                                        </Form.Item>
                                        <Form.Item name='service' label='Service' rules={[{ required: true }]}>
                                            <Select
                                                value={this.state.serviceSelected}
                                                onChange={this.onServiceChange}
                                                placeholder='Select service'>
                                                {this.state.service.map((data, index) => {
                                                    return (
                                                        <Option value={data.serviceId} key={data.serviceId}>
                                                            {data.name}
                                                        </Option>
                                                    )
                                                })}
                                            </Select>
                                        </Form.Item>
                                        <Form.Item name='employee' label='Employee' rules={[{ required: true }]}>
                                            <Select
                                                value={this.state.employeeSelected}
                                                onChange={this.onEmployeeChange}
                                                placeholder='Select employee'>
                                                {this.state.employee.map((data, index) => {
                                                    return (
                                                        <Option value={data.employeeId} key={data.employeeId}>
                                                            {data.name}
                                                        </Option>
                                                    )
                                                })}
                                            </Select>
                                        </Form.Item>
                                        <Form.Item name='appointmentDate' label='Date' rules={[{ required: true }]}>
                                            <Select
                                                value={this.state.selectedDate}
                                                onChange={this.onScheduleChange}
                                                placeholder='Schedule'>
                                                {this.state.employeeScheduleDate.map((data, index) => {
                                                    return (
                                                        <Option value={data.scheduleId} key={data.scheduleId}>
                                                            {data.textDate}
                                                        </Option>
                                                    )
                                                })}
                                            </Select>
                                        </Form.Item>
                                        <Form.Item
                                            name='appointmentDuration'
                                            label='Pick a Time'
                                            rules={[{ required: true }]}>
                                            <Select
                                                value={this.state.selectedPickATime}
                                                onChange={this.onPickTimeChange}
                                                placeholder='Pick Time'>
                                                {this.state.pickTime.map((data, index) => {
                                                    return (
                                                        <Option value={data.pickTimeId} key={data.pickTimeId}>
                                                            {moment(data.timeStamp).format('MM-DD HH:mm a')}
                                                        </Option>
                                                    )
                                                })}
                                            </Select>
                                        </Form.Item>
                                        <Form.Item label={<span>Notes</span>}>
                                            {getFieldDecorator('notes', {})(<Input placeholder='Please input notes' />)}
                                        </Form.Item>
                                        <Form.Item {...tailLayout}>
                                            <Button type='primary' htmlType='submit'>
                                                Add
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'new_appointment_view' })(FromView)

export default WrappedNormalLoginForm
