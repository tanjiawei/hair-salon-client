import '@/style/view-style/form.scss'

import axios from '@/api'
import { API } from '@/api/config'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Button, Col, Form, Input, Layout, message, Row, Select } from 'antd'
import QRCode from 'qrcode.react'
import React, { Component } from 'react'

import AppImagesList from '../../../components/renderImages/AppImagesList'

class CentralMerchandise extends Component {
    state = {
        _id: '',
        name: '',
        sku: '',
        price: 0,
        stock: 0,
        minStock: 0,
        note: '',
        imgURL: [],
        categoryId: '',
        priceWithPoint: '',
        point: 0,
        weight: '',
        categoryList: [],
        imgFiles: []
    }

    componentDidMount = async () => {
        await this.populateDataFromServer()
    }

    isNew = () => !this.state._id

    populateDataFromServer = async () => {
        try {
            const merchandiseId = new URLSearchParams(this.props.location.search).get('_id')
            const { data, status } = await axios.get(`${API}/centralmerchandise/` + (merchandiseId ?? ''))
            if (status !== 200) return

            this.setState({ ...data })
        } catch (error) {
            this.setState({ _id: '' })
        } finally {
            const { data: categoryList, status } = await axios.get(`${API}/CentralMerchandiseCategory`)
            if (status !== 200) return

            this.setState({ categoryList })
        }
    }

    handleImageChange = e => {
        const imgFiles = [...this.state.imgFiles]
        const FileList = e.target.files
        Object.keys(FileList).map(file => (file !== 'length' ? imgFiles.push(FileList[file]) : null))
        this.setState({ imgFiles })
    }

    handleDelete = source => i => {
        if (!window.confirm('Confirmed to remove the image?')) return
        const newSource = [...this.state[source]]
        newSource.splice(i, 1)
        this.setState({ [source]: newSource })
    }

    handleSubmit = e => {
        const { _id, imgFiles } = this.state
        const method = this.isNew() ? 'post' : 'put'

        e.preventDefault()
        this.props.form.validateFieldsAndScroll(async (err, fieldsValue) => {
            if (err) return
            const { name, sku, categoryId, price, priceWithPoint, point, weight, stock, minStock, note } = fieldsValue

            const data = {
                name,
                sku,
                categoryId,
                price,
                priceWithPoint,
                point,
                weight,
                stock,
                minStock,
                note,
                imgURL: JSON.stringify([...this.state.imgURL])
            }
            const postData = new FormData()

            Object.keys(data).map(d => postData.append(d, data[d]))
            imgFiles.map(img => postData.append('img', img))

            imgFiles.length = 0

            try {
                const { status } = await axios[method](`${API}/centralmerchandise/${_id.toString()}`, postData)
                if (status === 200) window.location = '/centralmerchandise'
            } catch (error) {
                message.error('An error appeared, please try again!')
            }
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const {
            _id,
            sku,
            name,
            price,
            stock,
            minStock,
            note,
            categoryId,
            imgFiles,
            categoryList,
            imgURL,
            priceWithPoint,
            point,
            weight
        } = this.state

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb
                        arr={['Merchandise Management', `${this.isNew() ? 'Add' : 'Edit'} Merchandise`]}
                    />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label={<span>Name</span>}>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please input name' }],
                                        initialValue: name
                                    })(<Input placeholder='Please input name' />)}
                                </Form.Item>

                                <Form.Item label={<span>SKU</span>}>
                                    {getFieldDecorator('sku', {
                                        rules: [{ required: false, message: 'Please input SKU (optional)' }],
                                        initialValue: sku
                                    })(<Input placeholder='Please input SKU (optional)' value={''} />)}
                                </Form.Item>
                                {sku && (
                                    <Form.Item label={<span>SKU QRCode</span>}>
                                        <QRCode value={sku} />
                                    </Form.Item>
                                )}
                                <Form.Item label='Category'>
                                    {getFieldDecorator('categoryId', {
                                        rules: [{ required: true }],
                                        initialValue: categoryId
                                    })(
                                        <Select name='categoryId' id='categoryId'>
                                            {categoryList.map(entry => (
                                                <option key={entry._id} value={entry._id}>
                                                    {entry.name}
                                                </option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>

                                <Form.Item label={<span>Price</span>}>
                                    {getFieldDecorator('price', {
                                        rules: [{ required: true, message: 'Please input price' }],
                                        initialValue: `${price}`
                                    })(<Input type='number' min={0} step={0.01} placeholder='Please input price' />)}
                                </Form.Item>

                                <Form.Item label={<span>Price With Point</span>}>
                                    {getFieldDecorator('priceWithPoint', {
                                        rules: [{ message: 'Please input price with point' }],
                                        initialValue: `${priceWithPoint}`
                                    })(
                                        <Input
                                            type='number'
                                            min={0}
                                            step={0.01}
                                            placeholder='Please input price with point'
                                        />
                                    )}
                                </Form.Item>

                                <Form.Item label={<span>Point</span>}>
                                    {getFieldDecorator('point', {
                                        rules: [{ message: 'Please input point' }],
                                        initialValue: `${point}`
                                    })(<Input type='number' min={0} step={1} placeholder='Please input point' />)}
                                </Form.Item>
                                <Form.Item label={<span>Weight (kg)</span>}>
                                    {getFieldDecorator('weight', {
                                        rules: [{ required: true, message: 'Please input weight' }],
                                        initialValue: `${weight}`
                                    })(<Input type='number' min={0} step={0.01} placeholder='Please input weight' />)}
                                </Form.Item>
                                <Form.Item label={<span>Stock</span>}>
                                    {getFieldDecorator('stock', {
                                        rules: [{ required: true, message: 'Please input stock' }],
                                        initialValue: `${stock}`
                                    })(
                                        <Input
                                            type='number'
                                            min={0}
                                            step={1}
                                            placeholder='Please input stock'
                                            disabled={Boolean(_id)}
                                        />
                                    )}
                                </Form.Item>
                                <Form.Item label={<span>Minimal Stock</span>}>
                                    {getFieldDecorator('minStock', {
                                        rules: [{ message: 'Please input minimum stock' }, { required: true }],
                                        initialValue: `${minStock}`
                                    })(<Input type='number' min={0} step={1} placeholder='Please input stock' />)}
                                </Form.Item>
                                <Form.Item label={<span>Note</span>}>
                                    {getFieldDecorator('note', {
                                        initialValue: `${note}`
                                    })(<Input.TextArea rows={4} autoSize placeholder='Please input note' />)}
                                </Form.Item>

                                <Form.Item label={<span>Picture</span>}>
                                    <Button>
                                        <input type='file' multiple onChange={this.handleImageChange} />
                                    </Button>
                                </Form.Item>
                                <Col>
                                    <div className='base-style'>
                                        {imgFiles[0] && (
                                            <AppImagesList
                                                srcFormat={item => URL.createObjectURL(item)}
                                                source={imgFiles}
                                                onClickContainer={this.handleDelete('imgFiles')}
                                                title='Pending to be uploaded'
                                            />
                                        )}
                                        {!this.isNew() && imgURL[0] && (
                                            <AppImagesList
                                                srcFormat={item => API + item.slice(6, 35)}
                                                source={imgURL}
                                                onClickContainer={this.handleDelete('imgURL')}
                                                title='Pictures'
                                            />
                                        )}
                                    </div>
                                </Col>
                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit' className='mr-5'>
                                        Save
                                    </Button>
                                    <Button type='secondary' onClick={() => (window.location = '/centralmerchandise')}>
                                        Go Back
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(CentralMerchandise)

export default WrappedNormalLoginForm
