import '@/style/view-style/table.scss'

import axios from '@/api'
import { API } from '@/api/config'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Button, Col, Layout, message, Pagination, Row, Table } from 'antd'
import moment from 'moment'
import React, { Component } from 'react'

class CentralInventoryLogList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            totalCount: 0,
            logs: []
        }

        props.onReSetCheckAdmin()
    }

    getData = async (pageNumber = 1, totalSize = 10) => {
        try {
            const {
                data: { totalCount, logs },
                status
            } = await axios.get(`${API}/CentralInventoryLog?pageNumber=${pageNumber}&pageSize=${totalSize}`)

            if (status !== 200) return message.error('There is an error on retrieving data.')
            this.setState({ totalCount, logs })
        } catch (error) {
            console.log(error.message)
        }
    }

    componentDidMount = async () => {
        await this.getData()
    }

    addButton = () => {
        this.props.history.push({
            pathname: '/centralinventoryLog/add'
        })
    }

    render() {
        const { logs, totalCount } = this.state
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Inventory Log List']} />
                </div>
                <div style={{ float: 'right', paddingBottom: '1rem' }}>
                    <Button style={{}} type='primary' htmlType='submit' onClick={this.addButton}>
                        Record Inventory Change
                    </Button>
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Merchandise Name',
                                        dataIndex: 'merchandise.name',
                                        key: 'merchandise.name'
                                    },
                                    {
                                        title: 'Stock Change',
                                        dataIndex: 'stockLog',
                                        key: 'stockLog',
                                        render: n => (n > 0 ? n : `( ${Math.abs(n)} )`)
                                    },
                                    {
                                        title: 'Total Stock',
                                        dataIndex: 'totalStock',
                                        key: 'totalStock'
                                    },
                                    {
                                        title: 'Order Id',
                                        dataIndex: 'orderId',
                                        key: 'orderId'
                                    },
                                    {
                                        title: 'Note',
                                        dataIndex: 'note',
                                        key: 'note'
                                    },
                                    {
                                        title: 'Date Created',
                                        dataIndex: 'dateIn',
                                        key: 'dateIn',
                                        render: date => moment(date).format('YYYY-MM-DD hh:mm:ss A')
                                    }
                                ]}
                                dataSource={logs}
                                pagination={{ hideOnSinglePage: true }}
                            />
                            <Pagination
                                onChange={this.getData}
                                total={totalCount}
                                className='d-flex justify-content-end mt-4'
                            />
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

export default CentralInventoryLogList
