import '@/style/view-style/table.scss'

import axios from '@/api'
import { API } from '@/api/config'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Badge, Col, Layout, Row, Table } from 'antd'
import React, { Component } from 'react'

class CentralShortageList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            entryList: [],
            entryCategory: []
        }
        props.onReSetCheckAdmin()
    }

    fetchData = async () => {
        try {
            const { data, status } = await axios.post(`${API}/centralmerchandise/getShortageList`)
            const { data: dataCategory, status: statusCategory } = await axios.get(`${API}/CentralMerchandiseCategory`)

            if (status !== 200 || statusCategory !== 200) return
            this.setState({ entryList: data, entryCategory: dataCategory })
        } catch (error) {
            console.log(error.message)
        }
    }

    componentDidMount = async () => {
        await this.fetchData()
    }

    render() {
        const { entryList, entryCategory } = this.state
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Shortage List']} />
                </div>
                <div style={{ marginBottom: '1rem' }}>
                    {entryList.length !== 0 ? (
                        <b>
                            Number of items in shortage: <Badge count={entryList.length} />
                        </b>
                    ) : (
                        <b>All Items are well stocked.</b>
                    )}
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Name',
                                        dataIndex: 'name',
                                        key: 'name'
                                    },
                                    {
                                        title: 'Category',
                                        dataIndex: 'categoryId',
                                        key: 'category',
                                        render: cId => {
                                            const result = entryCategory.filter(c => c._id === cId)
                                            return <span>{result.length === 0 ? 'No Category' : result[0].name}</span>
                                        }
                                    },
                                    {
                                        title: 'Stock',
                                        dataIndex: 'stock',
                                        key: 'stock'
                                    },
                                    {
                                        title: 'Minimal Stock',
                                        dataIndex: 'minStock',
                                        key: 'minStock'
                                    }
                                ]}
                                dataSource={entryList}
                            />
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

export default CentralShortageList
