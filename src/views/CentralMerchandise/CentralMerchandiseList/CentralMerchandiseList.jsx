import '@/style/view-style/table.scss'

import axios from '@/api'
import { API } from '@/api/config'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Button, Col, Input, Layout, Row, Table } from 'antd'
import React, { Component } from 'react'

import { altImageSource } from '../../../api/config'

class CentralMerchandiseList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            entryList: [],
            entryCategory: [],
            enterSku: '',
            searchedList: []
        }
        props.onReSetCheckAdmin()
    }

    editMerchandise(text) {
        this.props.history.push({
            pathname: '/centralmerchandise/EditMerchandise',
            search: '?_id=' + text._id.toString()
        })
    }

    deleteMerchandise = async _id => {
        if (!window.confirm('Please confirm the delete operation!')) return

        const originalEntryList = this.state.entryList

        const entryList = originalEntryList.filter(m => m._id !== _id)
        this.setState({ entryList })
        try {
            await axios.delete(`${API}/centralmerchandise/${_id}`)
        } catch (error) {
            this.setState({ entryList: originalEntryList })
        }
    }

    addButton = () => {
        this.props.history.push({
            pathname: '/centralmerchandise/addMerchandise'
        })
    }

    componentDidMount = async () => {
        await this.fetchMerchandiseList()
    }

    fetchMerchandiseList = async () => {
        try {
            const { data, status } = await axios.get(`${API}/centralmerchandise`)
            const { data: dataCategory, status: statusCategory } = await axios.get(`${API}/CentralMerchandiseCategory`)
            if (status !== 200 || statusCategory !== 200) return
            this.setState({ entryList: data, entryCategory: dataCategory })
        } catch (error) {
            console.log(error.message)
        }
    }

    handleSearchChange = ({ currentTarget }) => {
        this.setState({ enterSku: currentTarget.value })
    }

    getSearchData = () =>
        this.state.entryList.filter(a => a.sku.toLowerCase().includes(this.state.enterSku.toLowerCase()))

    render() {
        const { enterSku, entryList } = this.state
        const data = enterSku ? this.getSearchData() : entryList
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Merchandise List']} />
                </div>
                <div style={{ float: 'right', paddingBottom: '1rem' }}>
                    <Button type='primary' htmlType='submit' onClick={this.addButton}>
                        Add Merchandise
                    </Button>
                    <Input
                        style={{ width: '75%', float: 'right' }}
                        placeholder='Search by SKU ...'
                        // value={enterSku}
                        onChange={this.handleSearchChange}
                    />
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Preview',
                                        dataIndex: 'imgURL',
                                        key: 'imgURL',
                                        render: a =>
                                            a && a.length > 0 ? (
                                                <div className='central__merchandise__image--container'>
                                                    <img
                                                        src={API + a[a.length - 1].slice(6, 35)}
                                                        onError={e => (e.target.src = altImageSource)}
                                                        alt={a[0]}
                                                        className='central__merchandise__image--thumbnail'
                                                    />
                                                </div>
                                            ) : (
                                                'No images'
                                            )
                                    },
                                    {
                                        title: 'Name',
                                        dataIndex: 'name',
                                        key: 'name'
                                    },
                                    {
                                        title: 'Category',
                                        dataIndex: 'categoryId',
                                        key: 'category',
                                        render: cId => {
                                            const result = this.state.entryCategory.filter(c => c._id === cId)
                                            return <span>{result.length === 0 ? 'No Category' : result[0].name}</span>
                                        }
                                    },
                                    {
                                        title: 'Price',
                                        dataIndex: 'price',
                                        key: 'price',
                                        render: p => <span>${p.toFixed(2)}</span>
                                    },
                                    {
                                        title: 'Stock',
                                        dataIndex: 'stock',
                                        key: 'stock'
                                    },
                                    {
                                        title: 'Minimal Stock',
                                        dataIndex: 'minStock',
                                        key: 'minStock'
                                    },
                                    {
                                        title: 'Note',
                                        dataIndex: 'note',
                                        key: 'note'
                                    },
                                    {
                                        title: 'Action',
                                        key: 'action',
                                        render: (text, record) => (
                                            <span>
                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.editMerchandise(text)
                                                    }}>
                                                    Edit
                                                </Button>

                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.deleteMerchandise(text._id)
                                                    }}>
                                                    Delete
                                                </Button>
                                            </span>
                                        )
                                    }
                                ]}
                                dataSource={data}
                            />
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

export default CentralMerchandiseList
