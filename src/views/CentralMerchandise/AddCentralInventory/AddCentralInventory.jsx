import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, Select, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

class AddCentralInventory extends Component {
    state = {
        merchandiseList: []
    }

    componentDidMount = () => {
        this.fetchMerchandiseList()
    }

    fetchMerchandiseList = async () => {
        const { status, data: merchandiseList } = await axios.get(`${API}/centralmerchandise`)

        if (status !== 200) return null
        this.setState({ merchandiseList })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return

            const { merchandise, stockLog, note } = fieldsValue

            if (!window.confirm(`Confirm to change the stock by ${stockLog}?`)) return

            const postData = {
                merchandise,
                stockLog,
                note
            }
            axios
                .post(`${API}/CentralInventoryLog`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Add Success!')
                        window.location = '/centralinventoryLog'
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    message.error('Entry invalid!')
                    console.log(err)
                })
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Add Log']} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label='Merchandise Name'>
                                    {getFieldDecorator('merchandise', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please choose a merchandise'
                                            }
                                        ]
                                    })(
                                        <Select
                                            name='merchandise'
                                            id='merchandise'
                                            placeholder='Please choose a Merchandise'>
                                            {this.state.merchandiseList.map(entry => (
                                                <option value={entry._id}>{entry.name}</option>
                                            ))}
                                        </Select>
                                    )}
                                </Form.Item>

                                <Form.Item label={<span>Stock Change</span>}>
                                    {getFieldDecorator('stockLog', {
                                        rules: [{ required: true, message: 'Please input stock change' }]
                                    })(
                                        <Input
                                            type='number'
                                            min={-9999}
                                            step={1}
                                            placeholder='Please input stock change'
                                        />
                                    )}
                                </Form.Item>

                                <Form.Item label={<span>Note</span>}>
                                    {getFieldDecorator('note', {
                                        rules: [{ message: 'Please add a note (optional)' }]
                                    })(<Input placeholder='Please add a note (optional)' />)}
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Add
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(AddCentralInventory)

export default WrappedNormalLoginForm
