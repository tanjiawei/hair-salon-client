import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Table, Button, Select, message } from 'antd'
import '@/style/view-style/table.scss'
import axios from '@/api'
import { API, altImageSource } from '@/api/config'

const serverRoute = 'CentralAppCarousal'

class CentralAppCarousal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            pendingData: null,
            availableData: []
        }
        props.onReSetCheckAdmin()
    }

    componentDidMount = async () => {
        try {
            const data = await this.getMerchandiseCarousalList()
            const provided = data.map(m => m.merchandise._id)
            await this.getMerchandiseAvailableList(provided)
        } catch (error) {
            console.log(error.message)
        }
    }

    getMerchandiseCarousalList = async () => {
        const { data, status } = await axios.get(`${API}/${serverRoute}`)
        if (status !== 200) return

        this.setState({ data })
        return data
    }

    getMerchandiseAvailableList = async provided => {
        const data = {
            currentList: provided
        }
        const { data: availableData, status } = await axios.post(`${API}/${serverRoute}/available`, data)
        if (status !== 200) return

        this.setState({ availableData })
    }

    editMerchandise = async obj => {
        if (!window.confirm('Please confirm the Save operation!')) return
        const data = [...this.state.data]
        const index = data.indexOf(obj)
        try {
            const { status } = await axios.put(`${API}/${serverRoute}/${obj._id}`, {
                imageIndex: data[index].imageIndex
            })
            if (status === 200) await this.getMerchandiseCarousalList()
            message.success('Update Success')
        } catch (error) {
            window.location.reload()
        }
    }

    deleteMerchandise = async input => {
        if (!window.confirm('Please confirm the Delete operation!')) return

        const originalData = this.state.data

        const data = originalData.filter(m => m._id !== input._id)

        this.setState({ data })
        try {
            await axios.delete(`${API}/${serverRoute}/${input._id}`)
            await this.getMerchandiseAvailableList(this.state.data.map(m => m.merchandise._id))
        } catch (error) {
            this.setState({ data: originalData })
        }
    }

    handleSelectImageIndex = obj => imgIndex => {
        const data = [...this.state.data]
        const index = data.indexOf(obj)
        data[index] = { ...obj }
        data[index].imageIndex = imgIndex

        this.setState({ data })
    }

    handleChooseItem = index => {
        this.setState({ pendingData: this.state.availableData[index - 1] })
    }

    addMerchandise = async obj => {
        if (!window.confirm('Please confirm the Add operation!')) return

        const data = {
            merchandise: this.state.pendingData._id,
            imageIndex: 1
        }
        this.setState({ pendingData: null })
        try {
            await axios.post(`${API}/${serverRoute}`, data)
            await this.getMerchandiseCarousalList()
            await this.getMerchandiseAvailableList(this.state.data.map(m => m.merchandise._id))
        } catch (error) {
            message.error('An error appeared, please try again!')
        }
    }

    render() {
        const { data, availableData, pendingData: PD } = this.state
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Carousal Management']} />
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Name',
                                        dataIndex: 'merchandise.name',
                                        key: 'name'
                                    },
                                    {
                                        title: 'Preview',
                                        dataIndex: '',
                                        key: 'merchandise',
                                        render: obj =>
                                            obj.merchandise && obj.merchandise.imgURL[0] ? (
                                                <div className='central__merchandise__image--container'>
                                                    <img
                                                        src={
                                                            API +
                                                            obj.merchandise.imgURL[obj.imageIndex - 1].slice(6, 35)
                                                        }
                                                        alt={obj}
                                                        onError={e => (e.target.src = altImageSource)}
                                                        className='central__merchandise__image--thumbnail'
                                                    />
                                                </div>
                                            ) : (
                                                'No images'
                                            )
                                    },
                                    {
                                        title: 'Image Index',
                                        dataIndex: '',
                                        key: 'imageIndex',
                                        render: obj => {
                                            const imgURL = obj.merchandise.imgURL
                                            return (
                                                <Select
                                                    style={{ width: '75%' }}
                                                    value={obj.imageIndex}
                                                    onChange={this.handleSelectImageIndex(obj)}>
                                                    {imgURL[0] &&
                                                        imgURL.map((uri, index) => (
                                                            <Select.Option key={index} value={index + 1}>
                                                                {index + 1}
                                                            </Select.Option>
                                                        ))}
                                                </Select>
                                            )
                                        }
                                    },
                                    {
                                        title: 'Stock',
                                        dataIndex: 'merchandise.stock',
                                        key: 'stock'
                                    },
                                    {
                                        title: 'Stock Status',
                                        dataIndex: 'merchandise.isValid',
                                        key: 'isValid',
                                        render: bool =>
                                            bool ? (
                                                'OK'
                                            ) : (
                                                <b
                                                    title='Please remove from display.'
                                                    style={{ color: 'red', cursor: 'pointer' }}>
                                                    Invalid
                                                </b>
                                            )
                                    },
                                    {
                                        title: 'Action',
                                        key: 'action',
                                        render: object => (
                                            <div>
                                                <Button
                                                    type='primary'
                                                    style={{ marginRight: '1rem' }}
                                                    onClick={() => {
                                                        this.editMerchandise(object)
                                                    }}>
                                                    Save
                                                </Button>
                                                <Button
                                                    type='danger'
                                                    onClick={() => {
                                                        this.deleteMerchandise(object)
                                                    }}>
                                                    Delete
                                                </Button>
                                            </div>
                                        )
                                    }
                                ]}
                                dataSource={data}
                                pagination={false}
                            />
                            <div style={{ marginTop: '1rem' }}>
                                <Select
                                    style={{ width: '50%' }}
                                    value={PD ? `${PD.categoryId.name} __ ${PD.name} __ ${PD.stock}` : null}
                                    onChange={this.handleChooseItem}
                                    virtual>
                                    {availableData.map((m, index) => {
                                        if (!m.categoryId) m.categoryId = { name: 'NO CATEGORY' }
                                        return (
                                            <Select.Option key={index} value={index + 1}>
                                                {`${index + 1}. ${m.categoryId.name} __ ${m.name} __ ${m.stock}`}
                                            </Select.Option>
                                        )
                                    })}
                                </Select>
                                <Button
                                    style={{ marginLeft: '1rem' }}
                                    onClick={this.addMerchandise}
                                    disabled={!PD || availableData.length === 0 || data.length >= 8}>
                                    Add an item
                                </Button>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

export default CentralAppCarousal
