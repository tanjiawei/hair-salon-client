import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'
import AppImagesList from '@/components/renderImages/AppImagesList'
class EditMCategoryView extends Component {
    constructor(props) {
        super(props)
        // React Anti-pattern
        const { _id: defaultId, name: defaultName, imgLink: defaultImgLink } = props.location.state
        this.state = {
            _id: defaultId,
            name: defaultName,
            imgLink: defaultImgLink,
            imgFiles: []
        }
    }

    handleSubmit = e => {
        e.preventDefault()
        const { _id, imgFiles } = this.state
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return

            const postData = new FormData()
            postData.append('_id', _id)
            postData.append('name', fieldsValue.name)
            postData.append('img', imgFiles[0])

            axios
                .put(`${API}/merchandiseCategory/updateCategory`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Edit Success!')
                        window.location = '/merchandise/categoryList'
                    } else console.log(res)
                })
                .catch(console.log)
        })
    }

    handleImageChange = ({ target }) => this.setState({ imgFiles: [target.files[0]] })

    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }
        const { imgFiles, imgLink } = this.state
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Edit Category']} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label={<span>Name</span>}>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please input name' }],
                                        initialValue: `${this.state.name}`
                                    })(<Input placeholder='Please input name' value={'fsdfs'} />)}
                                </Form.Item>
                                <Form.Item label={<span>Picture</span>}>
                                    <Button>
                                        <input type='file' onChange={this.handleImageChange} />
                                    </Button>
                                </Form.Item>
                                <Col>
                                    <div className='base-style'>
                                        {imgFiles[0] && (
                                            <AppImagesList
                                                contained='false'
                                                srcFormat={item => URL.createObjectURL(item)}
                                                source={imgFiles}
                                                title='Pending to be uploaded and replaced the old image'
                                            />
                                        )}
                                        {imgLink && (
                                            <AppImagesList
                                                srcFormat={item => API + item.slice(6, 35)}
                                                source={[imgLink]}
                                                title='Pictures'
                                                contained='false'
                                            />
                                        )}
                                    </div>
                                </Col>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Save
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'edit_merchandise_category' })(EditMCategoryView)

export default WrappedNormalLoginForm
