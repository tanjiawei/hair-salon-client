import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'
import { getCheckId } from '../../../utils/localStorageFunc'
import AppImagesList from '@/components/renderImages/AppImagesList'
class AddMCategoryView extends Component {
    state = {
        imgFiles: []
    }

    handleSubmit = e => {
        e.preventDefault()
        const { imgFiles } = this.state
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return

            const postData = new FormData()
            postData.append('name', fieldsValue.name)
            postData.append('img', imgFiles[0])
            postData.append('adminid', getCheckId().adminId)

            axios
                .post(`${API}/merchandiseCategory/addCategory`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Add Success!')
                        window.location = '/merchandise/categoryList'
                    } else console.log(res)
                })
                .catch(console.log)
        })
    }

    handleImageChange = ({ target }) => this.setState({ imgFiles: [target.files[0]] })

    render() {
        const { imgFiles } = this.state
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Add Category']} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label={<span>Name</span>}>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please input name' }]
                                    })(<Input placeholder='Please input name' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Picture</span>}>
                                    <Button>
                                        <input type='file' onChange={this.handleImageChange} />
                                    </Button>
                                </Form.Item>

                                <Col>
                                    <div className='base-style'>
                                        {imgFiles[0] && (
                                            <AppImagesList
                                                contained='false'
                                                srcFormat={item => URL.createObjectURL(item)}
                                                source={imgFiles}
                                                title='Pending to be uploaded and replaced the old image'
                                            />
                                        )}
                                    </div>
                                </Col>
                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Add
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'add_merchandise_category' })(AddMCategoryView)

export default WrappedNormalLoginForm
