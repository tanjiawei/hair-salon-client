import React, { Component } from 'react'
import axios from '@/api'

import { API, altImageSource } from '@/api/config'
import { getCheckId } from '@/utils/localStorageFunc'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Table, Button, message } from 'antd'
import '@/style/view-style/table.scss'

class MCList extends Component {
    state = {
        data: []
    }

    componentDidMount = () => {
        axios
            .get(`${API}/merchandiseCategory/getAllCategory?id=${getCheckId().adminId}`)
            .then(({ status, data }) => status === 200 && this.setState({ data }))
            .catch(err => message.error(err))
    }

    editCategory = ({ _id, name, imgLink }) => {
        this.props.history.push({
            pathname: '/merchandise/EditCategory',
            search: '?_id=' + _id,
            state: { _id, name, imgLink }
        })
    }

    deleteCategory = _id => {
        if (!window.confirm('Please confirm the delete operation!')) return

        const original = this.state.data

        this.setState({ data: original.filter(c => c._id !== _id) })
        axios
            .delete(`${API}/merchandiseCategory/deleteCategory/${_id}`)
            .then(({ status }) => status === 200 && message.success('Delete Success!'))
            .catch(() => {
                message.error('Delete Failed!')
                this.setState({ data: original })
            })
    }

    addButton = () =>
        this.props.history.push({
            pathname: '/merchandise/AddCategory'
        })

    render() {
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Category List']} />
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Preview',
                                        dataIndex: 'imgLink',
                                        key: 'imgLink',
                                        render: a =>
                                            a && a.length > 0 ? (
                                                <div className='central__merchandise__image--container'>
                                                    <img
                                                        src={API + a.slice(6, 35)}
                                                        onError={e => (e.target.src = altImageSource)}
                                                        alt={a}
                                                        className='central__merchandise__image--thumbnail'
                                                    />
                                                </div>
                                            ) : (
                                                'No images'
                                            )
                                    },
                                    {
                                        title: 'Name',
                                        dataIndex: 'name',
                                        key: 'name'
                                    },
                                    {
                                        title: 'Action',
                                        key: 'action',
                                        render: (text, record) => (
                                            <span>
                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.editCategory(text)
                                                    }}>
                                                    Edit
                                                </Button>

                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.deleteCategory(text._id)
                                                    }}>
                                                    Delete
                                                </Button>
                                            </span>
                                        )
                                    }
                                ]}
                                dataSource={this.state.data}
                            />
                        </div>
                    </Col>
                </Row>
                <div style={{ float: 'right' }}>
                    <Button style={{}} type='primary' htmlType='submit' onClick={this.addButton}>
                        Add Category
                    </Button>
                </div>
            </Layout>
        )
    }
}

export default MCList
