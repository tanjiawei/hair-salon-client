import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, Select, message, Tabs, Tree } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'
import { getCheckId } from '../../../utils/localStorageFunc'

const { TabPane } = Tabs
const { TreeNode } = Tree

class FromView extends Component {
    constructor(props) {
        super(props)
        let _id, name, duration, price, description, categoryId, roomId

        let service = JSON.parse(localStorage.getItem('service'))

        _id = service._id
        name = service.name
        duration = service.duration
        price = service.price
        description = service.description
        categoryId = service.categoryId
        roomId = service.roomId
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            visible: true,
            _id: { _id },
            name: { name },
            price: { price },
            description: { description },
            duration: { duration },
            categoryList: [],
            categoryId: categoryId,
            roomList: [],
            roomId: roomId,
            merchandisList: {},
            merchandise: [],
            merchandiseBond: [],
            checkedKeys: []
        }
    }

    componentDidMount() {
        this.getServiceCategoryList()
        this.getRoomList()
        this.getMerchandiseList()
        this.getMerchandiseBond()
    }

    getMerchandiseBond = () => {
        axios
            .get(`${API}/service/getServiceMerchandiseBond/${this.state._id._id}`)
            .then(res => {
                if (res.status === 200) {
                    let data = res.data
                    this.setState({ merchandiseBond: data })

                    let checkedKeyList = []
                    data.forEach(bond => {
                        checkedKeyList.push(bond.merchandiseId)
                    })
                    this.setState({ checkedKeys: checkedKeyList })
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    saveMerchandiseBond = e => {
        e.preventDefault()
        let postData = {
            serviceId: this.state._id._id,
            bondList: []
        }
        this.state.checkedKeys.forEach(key => {
            if (this.state.merchandise.includes(key)) {
                postData.bondList.push({
                    merchandiseId: key,
                    usage: -document.getElementById(key).value
                })
            }
        })
        axios.delete(`${API}/service/deleteServiceMerchandiseBond/${this.state._id._id}`)

        axios
            .post(`${API}/service/addServiceMerchandiseBond`, postData)
            .then(res => {
                if (res.status === 200) {
                    message.success('Save Success!')
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                console.log(err)
            })
    }

    getMerchandiseList = () => {
        axios
            .get(`${API}/merchandise/getAllMerchandise?adminid=${getCheckId().adminId}`)
            .then(res => {
                if (res.status === 200) {
                    let data = {}
                    let merchandise = []
                    res.data.data.forEach(entry => {
                        if (entry.isValid && entry.isForService) {
                            merchandise.push(entry._id)
                            if (entry.categoryId.name in data) {
                                data[entry.categoryId.name].push({
                                    _id: entry._id,
                                    name: entry.name
                                })
                            } else {
                                data[entry.categoryId.name] = [
                                    {
                                        _id: entry._id,
                                        name: entry.name
                                    }
                                ]
                            }
                        }
                    })
                    this.setState({ merchandise: merchandise })
                    this.setState({ merchandisList: data })
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    getCheckedKey = () => {
        let checkedKeyList = []
        this.state.merchandiseBond.forEach(bond => {
            checkedKeyList.push(bond.merchandiseId)
        })
        return checkedKeyList
    }

    getUsage = id => {
        for (let i = 0; i < this.state.merchandiseBond.length; i++) {
            if (this.state.merchandiseBond[i].merchandiseId === id) {
                return -this.state.merchandiseBond[i].usage
            }
        }
    }

    onCheck = checkedKeys => {
        console.log(checkedKeys)
        this.setState({ checkedKeys })
    }

    renderTreeNodes = data =>
        Object.keys(data).map(key => {
            return (
                <TreeNode title={key}>
                    {data[key].map(entry => {
                        return (
                            <TreeNode
                                title={
                                    <div>
                                        <span>{entry.name}:</span>
                                        <span>
                                            <Input
                                                id={entry._id}
                                                size='small'
                                                placeholder='Consumption'
                                                defaultValue={this.getUsage(entry._id)}></Input>
                                        </span>
                                    </div>
                                }
                                key={entry._id}
                                checkable={true}
                            />
                        )
                    })}
                </TreeNode>
            )
        })

    getServiceCategoryList = () => {
        axios
            .get(`${API}/HRSCategory/getAllHRSCategory?id=${getCheckId().adminId}`)
            .then(res => {
                if (res.status === 200) {
                    var data = []
                    for (let i = 0; i < res.data.length; i++) {
                        data.push({
                            key: i,
                            _id: res.data[i]._id,
                            name: res.data[i].name
                        })
                    }
                    this.setState({ categoryList: data })
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    getRoomList = () => {
        axios
            .get(`${API}/room/getAllRoom?id=${getCheckId().adminId}`)
            .then(res => {
                if (res.status === 200) {
                    var data = []
                    for (let i = 0; i < res.data.length; i++) {
                        data.push({
                            key: i,
                            _id: res.data[i]._id,
                            name: res.data[i].name
                        })
                    }
                    this.setState({ roomList: data })
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    handleClose = () => {
        this.setState({ visible: false })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return
            const values = {
                ...fieldsValue,
                'date-picker': fieldsValue['date-picker'] ? fieldsValue['date-picker'].format('YYYY-MM-DD') : ''
            }
            let { _id, name, price, description, duration, categoryId, roomId } = values
            _id = this.state._id

            var postData = {
                _id: _id,
                name: name,
                price: price,
                description: description,
                duration: duration,
                categoryId: categoryId,
                roomId: roomId
            }
            axios
                .post(`${API}/service/updateService`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Save Success!')
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    console.log(err)
                })
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Services', 'Edit Services']}></CustomBreadcrumb>
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Tabs defaultActiveKey='0' size={'small'}>
                                <TabPane tab='Service Info' key='1'>
                                    <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                        <Form.Item label={<span>Name</span>}>
                                            {getFieldDecorator('name', {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: 'Please input name'
                                                    }
                                                ],
                                                initialValue: `${this.state.name.name}`
                                            })(<Input placeholder='Please input name' />)}
                                        </Form.Item>

                                        <Form.Item label={<span>Duration</span>}>
                                            {getFieldDecorator('duration', {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: 'Please input duration'
                                                    }
                                                ],
                                                initialValue: `${this.state.duration.duration}`
                                            })(<Input placeholder='Please input duration' />)}
                                        </Form.Item>
                                        <Form.Item label={<span>Price</span>}>
                                            {getFieldDecorator('price', {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: 'Please input service price'
                                                    }
                                                ],
                                                initialValue: `${this.state.price.price}`
                                            })(<Input placeholder='Please input service price' />)}
                                        </Form.Item>

                                        <Form.Item label={<span>Description</span>}>
                                            {getFieldDecorator('description', {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: 'Please input service description'
                                                    }
                                                ],
                                                initialValue: this.state.description.description
                                                    ? this.state.description.description
                                                    : ''
                                            })(<Input placeholder='Please input service description' />)}
                                        </Form.Item>

                                        <Form.Item label='Category'>
                                            {getFieldDecorator('categoryId', {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: 'Please choose a category'
                                                    }
                                                ],
                                                initialValue: this.state.categoryId
                                            })(
                                                <Select name='categoryId' id='categoryId'>
                                                    {this.state.categoryList.map(entry => (
                                                        <option value={entry._id}>{entry.name}</option>
                                                    ))}
                                                </Select>
                                            )}
                                        </Form.Item>

                                        <Form.Item label='Room'>
                                            {getFieldDecorator('roomId', {
                                                // rules: [
                                                //     {
                                                //         required: true,
                                                //         message: 'Please choose a room'
                                                //     }
                                                // ],
                                                initialValue: this.state.roomId
                                            })(
                                                <Select name='roomId' id='roomId'>
                                                    {this.state.roomList.map(entry => (
                                                        <option value={entry._id}>{entry.name}</option>
                                                    ))}
                                                </Select>
                                            )}
                                        </Form.Item>

                                        <Form.Item {...tailFormItemLayout}>
                                            <Button type='primary' htmlType='submit'>
                                                Save
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </TabPane>

                                <TabPane tab='Material Consumption' key='2'>
                                    <Tree
                                        checkable={true}
                                        onCheck={this.onCheck}
                                        checkedKeys={this.state.checkedKeys}
                                        defaultExpandAll={true}>
                                        {this.renderTreeNodes(this.state.merchandisList)}
                                    </Tree>

                                    <Button
                                        style={{ marginTop: 10, marginLeft: 10 }}
                                        type='primary'
                                        htmlType='submit'
                                        onClick={this.saveMerchandiseBond}>
                                        Save
                                    </Button>
                                </TabPane>
                            </Tabs>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(FromView)

export default WrappedNormalLoginForm
