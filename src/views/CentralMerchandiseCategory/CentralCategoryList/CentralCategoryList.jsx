import '@/style/view-style/table.scss'

import axios from '@/api'
import { altImageSource, API } from '@/api/config'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Button, Col, Layout, message, Row, Table } from 'antd'
import React, { Component } from 'react'

class CentralCategoryList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
        props.onReSetCheckAdmin()
    }

    async componentDidMount() {
        await this.fetchMerchandiseList()
    }

    editCategory(text) {
        this.props.history.push({
            pathname: '/CentralMerchandiseCategory/EditCategory',
            search: '?_id=' + text._id.toString(),
            state: {
                _id: text._id,
                name: text.name,
                imgLink: text.imgLink
            }
        })
    }

    deleteCategory = async _id => {
        if (!window.confirm('Please confirm the delete operation!')) return
        const originalData = this.state.data

        const data = originalData.filter(c => c._id !== _id)

        this.setState({ data })
        try {
            await axios.delete(`${API}/CentralMerchandiseCategory/${_id}`)
            message.success('Deleted Successfully.')
        } catch (error) {
            this.setState({ data: originalData })
            message.error('Something Failed.')
        }
    }

    addButton = () => {
        this.props.history.push({
            pathname: '/CentralMerchandiseCategory/AddCategory'
        })
    }

    fetchMerchandiseList = async () => {
        try {
            const { status, data } = await axios.get(`${API}/CentralMerchandiseCategory`)

            if (status !== 200) return
            this.setState({ data })
        } catch (error) {
            console.log(error.message)
        }
    }

    render() {
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', 'Category List']} />
                </div>
                <div style={{ float: 'right', paddingBottom: '1rem' }}>
                    <Button style={{}} type='primary' htmlType='submit' onClick={this.addButton}>
                        Add Category
                    </Button>
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Preview',
                                        dataIndex: 'imgLink',
                                        key: 'imgLink',
                                        render: a =>
                                            a && a.length > 0 ? (
                                                <div className='central__merchandise__image--container'>
                                                    <img
                                                        src={API + a.slice(6, 35)}
                                                        onError={e => (e.target.src = altImageSource)}
                                                        alt={a}
                                                        className='central__merchandise__image--thumbnail'
                                                    />
                                                </div>
                                            ) : (
                                                'No images'
                                            )
                                    },
                                    {
                                        title: 'Name',
                                        dataIndex: 'name',
                                        key: 'name'
                                    },
                                    {
                                        title: 'Action',
                                        key: 'action',
                                        render: (text, record) => (
                                            <span>
                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.editCategory(text)
                                                    }}>
                                                    Edit
                                                </Button>

                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.deleteCategory(text._id)
                                                    }}>
                                                    Delete
                                                </Button>
                                            </span>
                                        )
                                    }
                                ]}
                                dataSource={this.state.data}
                            />
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

export default CentralCategoryList
