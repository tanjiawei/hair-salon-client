import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, message } from 'antd'
import '@/style/view-style/form.scss'

import axios from '@/api'
import { API } from '@/api/config'
import AppImagesList from '../../../components/renderImages/AppImagesList'

class CentralCategory extends Component {
    constructor(props) {
        super(props)

        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            visible: true,
            _id: '',
            name: '',
            imgLink: '',
            imgFiles: []
        }
    }

    isNew = () => this.props.location.search.length === 0

    componentDidMount = async () => {
        if (this.isNew()) return
        await this.fetchCategoryById()
    }

    fetchCategoryById = async () => {
        const categoryId = new URLSearchParams(this.props.location.search).get('_id')

        const { data, status } = await axios.get(`${API}/CentralMerchandiseCategory/` + categoryId)

        if (status !== 200) return

        this.setState({ ...data })
    }

    handleImageChange = e => {
        this.setState({ imgFiles: [e.target.files[0]] })
    }

    handleDelete = source => i => {
        if (!window.confirm('Confirmed to remove the image?')) return
        const newSource = [...this.state[source]]
        newSource.splice(i, 1)
        this.setState({ [source]: newSource })
    }

    handleSubmit = e => {
        const { _id, imgFiles } = this.state
        const method = this.isNew() ? 'post' : 'put'
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return
            let { name } = fieldsValue

            const postData = new FormData()
            postData.append('name', name)
            postData.append('img', imgFiles[0])

            axios[method](`${API}/CentralMerchandiseCategory/${_id.toString()}`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Entry Success!')
                        window.location = '/centralMerchandiseCategory'
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    message.error('An error appeared, please try again!')
                })
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const { imgFiles, imgLink } = this.state

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchandise Management', `${this.isNew() ? 'Add' : 'Edit'} Category`]} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label={<span>Name</span>}>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please input name' }],
                                        initialValue: `${this.state.name}`
                                    })(<Input placeholder='Please input name' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Picture</span>}>
                                    <Button>
                                        <input type='file' onChange={this.handleImageChange} />
                                    </Button>
                                </Form.Item>

                                <Col>
                                    <div className='base-style'>
                                        {imgFiles[0] && (
                                            <AppImagesList
                                                srcFormat={item => URL.createObjectURL(item)}
                                                source={imgFiles}
                                                onClickContainer={this.handleDelete('imgFiles')}
                                                title='Pending to be uploaded and replaced the old image'
                                            />
                                        )}
                                        {!this.isNew() && imgLink && (
                                            <AppImagesList
                                                srcFormat={item => API + item.slice(6, 35)}
                                                source={[imgLink]}
                                                title='Pictures'
                                                contained='false'
                                            />
                                        )}
                                    </div>
                                </Col>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit' style={{ marginRight: '5rem' }}>
                                        Save
                                    </Button>
                                    <Button
                                        type='secondary'
                                        onClick={() => (window.location = '/CentralMerchandiseCategory')}>
                                        Go Back
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(CentralCategory)

export default WrappedNormalLoginForm
