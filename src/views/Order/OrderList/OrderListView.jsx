import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import moment from 'moment'
import { Layout, Row, Col, Button, Input, Table, message, Popconfirm, DatePicker } from 'antd'

import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

const { RangePicker } = DatePicker
const dateFormat = 'YYYY-MM-DD'
let todayFrom = new Date()
let todayTo = new Date()

class OrderListView extends Component {
    constructor(props) {
        super(props)
        todayFrom.setDate(todayTo.getDate() - 90)
        this.state = {
            orderData: [],
            showConfirm: false,
            searchOrderDateFrom: moment(todayFrom).format('YYYY-MM-DD'),
            searchOrderDateTo: moment(todayTo).format('YYYY-MM-DD'),
            searchOrderId: ''
        }
    }

    componentDidMount() {
        this.getOrderData(this.state.searchOrderDateFrom, this.state.searchOrderDateTo, '')
    }

    getOrderData = (dateFrom, dateTo, orderId) => {
        let data = []
        let postData = {}

        postData = {
            orderDateFrom: dateFrom,
            orderDateTo: dateTo,
            orderId: orderId
        }
        axios
            .post(`${API}/order/getAllCentralMerchandiseOrderByIdOrDate`, postData)
            .then(res => {
                if (res.status === 200) {
                    if (res.data.length > 0) {
                        for (let i = 0; i < res.data.length; i++) {
                            data.push({
                                _id: res.data[i]._id,
                                user: res.data[i].user,
                                centralMerchandiseItems: res.data[i].centralMerchandiseItems,
                                orderDate: moment(new Date(res.data[i].orderDate)).format('MMM DD, YYYY HH:mm'),
                                orderEventItems: res.data[i].orderEventItems,
                                orderId: res.data[i].orderId,
                                orderMerchandiseItems: res.data[i].orderMerchandiseItems,
                                orderSubtotal: res.data[i].orderSubtotal,
                                orderTax: res.data[i].orderTax,
                                orderDelivery: res.data[i].orderDelivery,
                                shipment: res.data[i].shipment,
                                orderAmount: res.data[i].orderAmount,
                                orderSubtotalPoint: res.data[i].orderSubtotalPoint,
                                shippingAddress: res.data[i].shippingAddress,
                                status: res.data[i].status,
                                type: res.data[i].type,
                                contact:
                                    res.data[i].user.name +
                                    (res.data[i].user.phone ? '\n' + res.data[i].user.phone : '') +
                                    (res.data[i].user.email ? '\n' + res.data[i].user.email : '')
                            })
                        }
                        this.setState({
                            orderData: data
                        })
                    }
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    handleCancel = e => {
        this.setState({ showConfirm: false })
    }

    cancelConfirm_onClick = event => {
        let data = [...this.state.orderData]

        var postData = {
            _id: event._id,
            status: 'CANCELED'
        }
        axios
            .post(`${API}/order/updateCentralMerchandiseStatus`, postData)
            .then(res => {
                if (res.status === 200) {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i]._id === event._id) {
                            data[i].status = 'CANCELED'
                        }
                    }
                    message.success('Cancel Success')
                }
                this.setState({
                    orderData: data,
                    changeStatusId: ''
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    search_onClick = () => {
        const orderDateFrom = this.state.searchOrderDateFrom
        const orderDateTo = this.state.searchOrderDateTo
        const orderId = this.state.searchOrderId

        this.getOrderData(orderDateFrom, orderDateTo, orderId)
    }

    setSearchOrderId = e => {
        this.setState({ searchOrderId: e.target.value })
    }

    searchOrderDate_onChange = (dates, dateStrings) => {
        this.setState({
            searchOrderDateFrom: dateStrings[0],
            searchOrderDateTo: dateStrings[1]
        })
    }

    render() {
        let columnsEvent = [
            {
                title: 'Order Id',
                dataIndex: 'orderId',
                key: 'orderId'
            },
            {
                title: 'Tax',
                dataIndex: 'orderTax',
                key: 'orderTax'
            },
            {
                title: 'Total',
                dataIndex: 'orderAmount',
                key: 'orderAmount'
            },
            {
                title: 'Delivery',
                dataIndex: 'orderDelivery',
                key: 'orderDelivery'
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status'
            },
            {
                title: 'Contact',
                dataIndex: 'contact',
                key: 'contact'
            },
            {
                title: 'Order Date',
                dataIndex: 'orderDate',
                key: 'orderDate',
                defaultSortOrder: 'descend',
                sorter: (a, b) => moment(a.orderDate).format('x') - moment(b.orderDate).format('x')
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <Popconfirm
                            title='Are you sure want to cancel?'
                            onConfirm={() => this.cancelConfirm_onClick(record)}
                            okText='Yes'
                            cancelText='No'>
                            <Button type='danger' disabled={record.status === 'CANCELED'}>
                                {record.status === 'CANCELED' ? 'Canceled' : 'Cancel'}
                            </Button>
                        </Popconfirm>
                    </span>
                )
            }
        ]

        return (
            <div>
                <Layout className='animated fadeIn'>
                    <div>
                        <CustomBreadcrumb arr={['Order', 'Order List']}></CustomBreadcrumb>
                    </div>
                    <div className='base-style'>
                        <div>
                            <div>
                                Order Date:{' '}
                                <RangePicker
                                    ranges={{
                                        Today: [moment(), moment()],
                                        'This Month': [moment().startOf('month'), moment().endOf('month')]
                                    }}
                                    allowClear={false}
                                    defaultValue={[moment(todayFrom, dateFormat), moment(todayTo, dateFormat)]}
                                    onChange={this.searchOrderDate_onChange}
                                />{' '}
                                Order Id:{' '}
                                <Input
                                    style={{ width: '200px' }}
                                    value={this.state.searchOrderId}
                                    onChange={this.setSearchOrderId}
                                />
                            </div>
                            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                                <Button type='primary' onClick={this.search_onClick}>
                                    Search
                                </Button>
                            </div>
                        </div>
                        <Row>
                            <Col span={24}>
                                <Table
                                    columns={columnsEvent}
                                    expandedRowRender={record =>
                                        record.centralMerchandiseItems.map(orderItem => (
                                            <div className='base-style' key={orderItem._id}>
                                                {record.shipment !== undefined
                                                    ? 'Shipment Id: ' + record.shipment.shipmentId
                                                    : ''}
                                                <br />
                                                <Row>
                                                    <Col span={3}>
                                                        <img
                                                            src={API + orderItem.imgURL[orderItem.imgURL.length - 1]}
                                                            alt='Order Item'
                                                            width='69px'
                                                            height='103px'></img>
                                                    </Col>
                                                    <Col span={10}>
                                                        <div>
                                                            {orderItem.items.name}
                                                            <br />
                                                            {orderItem.priceSelect === true
                                                                ? `$${orderItem.items.price}`
                                                                : `$${orderItem.items.priceWithPoint}+${orderItem.items.point}`}
                                                            <br />
                                                            Qty: {orderItem.quantity}
                                                            <br />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                        ))
                                    }
                                    dataSource={this.state.orderData}
                                    rowKey='_id'
                                    pagination={{ hideOnSinglePage: true }}
                                />
                            </Col>
                        </Row>
                    </div>
                </Layout>
            </div>
        )
    }
}

export default OrderListView
