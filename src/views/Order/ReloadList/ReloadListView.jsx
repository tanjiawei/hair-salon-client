import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import moment from 'moment'
import {
    Layout,
    Row,
    Col,
    Divider,
    Button,
    Input,
    Table,
    Typography,
    PageHeader,
    Descriptions,
    Modal,
    message
} from 'antd'

import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

class ReloadListView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            orderData: []
        }
    }

    componentDidMount() {
        this.getReloadData()
    }

    getReloadData = () => {
        // let data = []
        // axios
        //     .post(
        //         `${API}/order/getAllCentralMerchandiseReload`,
        //         {},
        //         {
        //             headers: {
        //                 Accept: 'application/json',
        //                 'Content-Type': 'application/json',
        //                 Authorization: 'Bearer ' + localStorage.getItem('jwt')
        //             }
        //         }
        //     )
        //     .then(res => {
        //         if (res.status === 200) {
        //             if (res.data.length > 0) {
        //                 for (let i = 0; i < res.data.length; i++) {
        //                     data.push({
        //                         _id: res.data[i]._id,
        //                         user: res.data[i].user,
        //                         centralMerchandiseItems: res.data[i].centralMerchandiseItems,
        //                         orderDate: moment(new Date(res.data[i].orderDate)).format('MMM DD, YYYY HH:mm'),
        //                         orderEventItems: res.data[i].orderEventItems,
        //                         orderId: res.data[i].orderId,
        //                         orderMerchandiseItems: res.data[i].orderMerchandiseItems,
        //                         orderSubtotal: res.data[i].orderSubtotal,
        //                         shippingAddress: res.data[i].shippingAddress,
        //                         status: res.data[i].status,
        //                         type: res.data[i].type
        //                     })
        //                 }
        //                 this.setState({
        //                     orderData: data
        //                 })
        //             }
        //         } else {
        //             console.log(res)
        //         }
        //     })
        //     .catch(err => {
        //         message.error(err)
        //     })
    }

    render() {
        let columnsEvent = [
            {
                title: 'Reload Id',
                dataIndex: 'orderId',
                key: 'orderId'
            },
            {
                title: 'Sub Total',
                dataIndex: 'orderSubtotal',
                key: 'orderSubtotal'
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status'
            },
            {
                title: 'Name',
                dataIndex: 'user.name',
                key: 'user.name'
            },
            {
                title: 'Phone',
                dataIndex: 'user.phone',
                key: 'user.phone'
            },
            {
                title: 'Email',
                dataIndex: 'user.email',
                key: 'user.email'
            },
            {
                title: 'Reload Date',
                dataIndex: 'orderDate',
                key: 'orderDate',
                defaultSortReload: 'descend',
                sorter: (a, b) => moment(a.orderDate).format('x') - moment(b.orderDate).format('x')
            }
        ]

        return (
            <div>
                <Layout className='animated fadeIn'>
                    <div>
                        <CustomBreadcrumb arr={['Reload', 'Reload List']}></CustomBreadcrumb>
                    </div>
                    <div className='base-style'>
                        {/* <Row>
                            <Col span={24}>
                                <Table
                                    columns={columnsEvent}
                                    expandedRowRender={record =>
                                        record.centralMerchandiseItems.map(orderItem => (
                                            <div className='base-style' key={orderItem._id}>
                                                <Row>
                                                    <Col span={3}>
                                                        <img
                                                            src={API + orderItem.imgURL[orderItem.imgURL.length - 1]}
                                                            alt='Reload Item'
                                                            width='69px'
                                                            height='103px'></img>
                                                    </Col>
                                                    <Col span={10}>
                                                        <div>
                                                            {orderItem.name}
                                                            <br />${orderItem.merchandiseItemTotal}
                                                            <br />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                        ))
                                    }
                                    dataSource={this.state.orderData}
                                    rowKey='_id'
                                    pagination={{ hideOnSinglePage: true }}
                                />
                            </Col>
                        </Row> */}
                    </div>
                </Layout>
            </div>
        )
    }
}

export default ReloadListView
