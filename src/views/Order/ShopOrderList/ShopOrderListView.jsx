import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import moment from 'moment'
import { Layout, Row, Col, Divider, Button, Input, Table, Modal, message, Select, Pagination } from 'antd'

import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

const { Option } = Select

let changeStatusDefaultValue = ''

class ShopOrderListView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            totalCount: 0,
            orderData: [],
            changeStatusRecordId: '',
            updateUserRecordId: '',
            searchUser: {
                email: '',
                phone: '',
                name: '',
                result: []
            }
        }
    }

    componentDidMount() {
        this.getShopOrderData()
    }

    getShopOrderData = (pageNumber = 1, totalSize = 10) => {
        axios
            .post(`${API}/order/getAllCentralMerchandiseShopOrder?pageNumber=${pageNumber}&pageSize=${totalSize}`, {})
            .then(({ status, data: { totalCount, orders } }) => {
                if (status !== 200) return message.error('There is an error on retrieving data.')
                const orderData = orders.map(o => ({
                    _id: o._id,
                    centralMerchandiseItems: o.centralMerchandiseItems,
                    orderAmount: o.orderAmount,
                    orderDate: moment(new Date(o.orderDate)).format('MMM DD, YYYY HH:mm'),
                    orderDelivery: o.orderDelivery,
                    orderEventItems: o.orderEventItems,
                    orderId: o.orderId,
                    orderMerchandiseItems: o.orderMerchandiseItems,
                    orderSubtotal: o.orderSubtotal,
                    orderSubtotalPoint: o.orderSubtotalPoint,
                    orderTax: o.orderTax,
                    tips: o.tipsAmount,
                    status: o.status,
                    type: o.type,
                    user: o.user,
                    contact: o.user
                        ? o.user.name +
                          (o.user.phone ? '\n' + o.user.phone : '') +
                          (o.user.email ? '\n' + o.user.email : '')
                        : ''
                }))
                this.setState({ orderData, totalCount })
                // for (let i = 0; i < res.data.length; i++) {
                //     data.push({
                //         _id: res.data[i]._id,
                //         centralMerchandiseItems: res.data[i].centralMerchandiseItems,
                //         orderAmount: res.data[i].orderAmount,
                //         orderDate: moment(new Date(res.data[i].orderDate)).format('MMM DD, YYYY HH:mm'),
                //         orderDelivery: res.data[i].orderDelivery,
                //         orderEventItems: res.data[i].orderEventItems,
                //         orderId: res.data[i].orderId,
                //         orderMerchandiseItems: res.data[i].orderMerchandiseItems,
                //         orderSubtotal: res.data[i].orderSubtotal,
                //         orderSubtotalPoint: res.data[i].orderSubtotalPoint,
                //         orderTax: res.data[i].orderTax,
                //         tips: res.data[i].tipsAmount,
                //         status: res.data[i].status,
                //         type: res.data[i].type,
                //         user: res.data[i].user,
                //         contact: res.data[i].user
                //             ? res.data[i].user.name +
                //               (res.data[i].user.phone ? '\n' + res.data[i].user.phone : '') +
                //               (res.data[i].user.email ? '\n' + res.data[i].user.email : '')
                //             : ''
                //     })

                //     this.setState({
                //         orderData: data
                //     })
                // }
            })
            .catch(message.log)
    }

    changeUpdateUser_onClick = record => {
        this.setState({
            updateUserRecordId: record._id
        })
    }

    updateUser_onOk = () => {
        if (this.state.searchUser.result.length > 0) {
            const userId = this.state.searchUser.result[0]._id
            let data = [...this.state.orderData]

            var postData = {
                _id: this.state.updateUserRecordId,
                userId: userId
            }
            axios
                .post(`${API}/order/updateOrderUserId`, postData)
                .then(res => {
                    if (res.status === 200) {
                        for (let i = 0; i < data.length; i++) {
                            if (data[i]._id === this.state.updateUserRecordId) {
                                data[i].user = this.state.searchUser.result[0]
                                break
                            }
                        }
                        message.success('Update Status Success')
                    }
                    this.setState({
                        orderData: data,
                        updateUserRecordId: ''
                    })
                })
                .catch(err => {
                    console.log(err)
                })
        }
    }

    updateUser_onCancel = () => {
        this.setState({
            updateUserRecordId: '',
            searchUser: {
                email: '',
                phone: '',
                name: '',
                result: []
            }
        })
    }

    changeStatusUpdate_onClick = record => {
        changeStatusDefaultValue = record.status
        this.setState({
            changeStatusRecordId: record._id
        })
    }

    changeStatus_onChange = value => {
        changeStatusDefaultValue = value
    }

    updateUserSearch_onClick = () => {
        let data
        axios
            .post(`${API}/user/getUserByEmailOrPhone`, {
                email: this.state.searchUser.email,
                phone: this.state.searchUser.phone
            })
            .then(res => {
                if (res.status === 200) {
                    if (res.data.length > 0) {
                        data = {
                            email: res.data[0].email,
                            phone: res.data[0].phone,
                            name: res.data[0].name,
                            result: res.data
                        }
                        this.setState({
                            searchUser: data
                        })
                    } else message.error('No user found')
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    changeStatus_onOk = () => {
        let data = [...this.state.orderData]
        var postData = {
            _id: this.state.changeStatusRecordId,
            status: changeStatusDefaultValue
        }
        axios
            .post(`${API}/order/updateCentralMerchandiseStatus`, postData)
            .then(res => {
                if (res.status === 200) {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i]._id === this.state.changeStatusRecordId) {
                            data[i].status = changeStatusDefaultValue
                            break
                        }
                    }
                    message.success('Update Status Success')
                }
                this.setState({
                    orderData: data,
                    changeStatusRecordId: ''
                })
                changeStatusDefaultValue = ''
            })
            .catch(err => {
                console.log(err)
            })
    }

    changeStatus_onCancel = () => {
        this.setState({
            changeStatusRecordId: ''
        })
    }

    email_onChange = event => {
        this.setState({
            searchUser: {
                email: event.target.value,
                phone: '',
                name: ''
            }
        })
    }

    phone_onChange = event => {
        this.setState({
            searchUser: {
                email: '',
                phone: event.target.value,
                name: ''
            }
        })
    }

    render() {
        let columnsEvent = [
            {
                title: 'Order Id',
                dataIndex: 'orderId',
                key: 'orderId'
            },
            {
                title: 'Sub Total',
                dataIndex: 'orderSubtotal',
                key: 'orderSubtotal'
            },
            {
                title: 'Tips Amount',
                dataIndex: 'tips',
                key: 'tips'
            },
            {
                title: 'Total',
                dataIndex: 'orderAmount',
                key: 'orderAmount'
            },
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status'
            },
            {
                title: 'Contact',
                dataIndex: 'contact',
                key: 'contact'
            },

            {
                title: 'Order Date',
                dataIndex: 'orderDate',
                key: 'orderDate',
                defaultSortOrder: 'descend',
                sorter: (a, b) => moment(a.orderDate).format('x') - moment(b.orderDate).format('x')
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <Button
                            type='link'
                            onClick={() => {
                                this.changeStatusUpdate_onClick(record)
                            }}>
                            Change Status
                        </Button>
                        <Modal
                            title='Status'
                            centered
                            width={300}
                            visible={this.state.changeStatusRecordId !== ''}
                            onOk={this.changeStatus_onOk}
                            onCancel={this.changeStatus_onCancel}>
                            <div>
                                <Select
                                    defaultValue={changeStatusDefaultValue}
                                    onChange={this.changeStatus_onChange}
                                    style={{ width: 200 }}>
                                    <Option value='WAITINGPAY'>WAITINGPAY</Option>
                                    <Option value='PENDING'>PENDING</Option>
                                    <Option value='PAID'>PAID</Option>
                                    <Option value='SHIPPED'>SHIPPED</Option>
                                    <Option value='RETURNED'>RETURNED</Option>
                                    <Option value='CANCELED'>CANCELED</Option>
                                </Select>
                            </div>
                        </Modal>
                        {record.user === undefined ? (
                            <Button
                                type='link'
                                disabled={record.user === undefined ? false : true}
                                onClick={() => {
                                    this.changeUpdateUser_onClick(record)
                                }}>
                                Choose User
                            </Button>
                        ) : null}

                        <Modal
                            title='User'
                            centered
                            width={300}
                            visible={this.state.updateUserRecordId !== ''}
                            onOk={this.updateUser_onOk}
                            onCancel={this.updateUser_onCancel}>
                            <div>
                                <Input
                                    placeholder='Search by email'
                                    value={this.state.searchUser.email}
                                    onChange={this.email_onChange}
                                />
                                <br />
                                <br />
                                <Input
                                    placeholder='Search by phone'
                                    value={this.state.searchUser.phone}
                                    onChange={this.phone_onChange}
                                />
                                <br />
                                <br />
                                <Button onClick={this.updateUserSearch_onClick}>Search</Button>
                                <Divider />
                                <p>Name: {this.state.searchUser.name}</p>
                            </div>
                        </Modal>
                    </span>
                )
            }
        ]

        const { totalCount, orderData } = this.state
        return (
            <div>
                <Layout className='animated fadeIn'>
                    <div>
                        <CustomBreadcrumb arr={['Order', 'Shop Order List']}></CustomBreadcrumb>
                    </div>
                    <div className='base-style'>
                        <Row>
                            <Col span={24}>
                                <Table
                                    columns={columnsEvent}
                                    expandedRowRender={record => (
                                        <div>
                                            {Object.keys(record.orderMerchandiseItems).map(key => (
                                                <div className='base-style' key={record.orderMerchandiseItems[key]._id}>
                                                    <Row>
                                                        <Col span={3} style={{ textAlign: 'right' }}>
                                                            <div>
                                                                Item:
                                                                <br />
                                                                Order Quantity:
                                                                <br />
                                                                Total:
                                                                <br />
                                                            </div>
                                                        </Col>
                                                        <Col span={1}></Col>
                                                        <Col span={10}>
                                                            <div>
                                                                {record.orderMerchandiseItems[key].name}
                                                                <br />
                                                                {record.orderMerchandiseItems[key].orderQuantity}
                                                                <br />
                                                                {record.orderMerchandiseItems[key].merchandiseItemTotal}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            ))}
                                            {Object.keys(record.orderEventItems).map(key => (
                                                <div className='base-style' key={record.orderEventItems[key]._id}>
                                                    <Row>
                                                        <Col span={3} style={{ textAlign: 'right' }}>
                                                            <div>
                                                                Name:
                                                                <br />
                                                                Status:
                                                                <br />
                                                                Employee:
                                                                <br />
                                                                Service:
                                                                <br />
                                                                Date:
                                                                <br />
                                                            </div>
                                                        </Col>
                                                        <Col span={1}></Col>
                                                        <Col span={10}>
                                                            <div>
                                                                {record.orderEventItems[key].username}
                                                                <br />
                                                                {record.orderEventItems[key].status}
                                                                <br />
                                                                {record.orderEventItems[key].selectedEmployee.name}
                                                                <br />
                                                                {record.orderEventItems[key].selectedService.name}
                                                                <br />
                                                                {moment(record.orderEventItems[key].day).format(
                                                                    'YYYY/MM/DD'
                                                                )}{' '}
                                                                {moment(record.orderEventItems[key].start).format(
                                                                    'HH:mm'
                                                                )}
                                                                -
                                                                {moment(record.orderEventItems[key].end).format(
                                                                    'HH:mm'
                                                                )}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            ))}
                                        </div>
                                    )}
                                    showQuickJumper
                                    dataSource={orderData}
                                    rowKey='_id'
                                    pagination={{ hideOnSinglePage: true }}
                                />
                                <Pagination
                                    onChange={this.getShopOrderData}
                                    total={totalCount}
                                    className='d-flex justify-content-end mt-4'
                                />
                            </Col>
                        </Row>
                    </div>
                </Layout>
            </div>
        )
    }
}

export default ShopOrderListView
