import React, { useState, useEffect } from 'react'
import { Layout, Row, Col } from 'antd'
import axios from '@/api'
import moment from 'moment'

import '@/style/view-style/login.scss'
import AppImagesList from '../../components/renderImages/AppImagesList'
import { HeartFilled } from '@ant-design/icons'
import { altImageSource } from '../../api/config'
import { API } from '../../api/config'

const TapView = ({ match }) => {
    const [feeding, setFeeding] = useState(null)

    useEffect(() => {
        populateDataFromServer()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const populateDataFromServer = async () => {
        const feedId = match.params._id
        try {
            const { data } = await axios.get(`${API}/feedings/` + feedId)
            setFeeding(data)
        } catch (error) {
            console.error(error.message)
            window.location = '/404'
        }
    }

    const gridHandler = n => ([1, 2, 4].includes(n) ? 12 : 8)
    const sizeHandler = n => gridHandler(n) * 12.5 + 'px'

    const likeColor = () => (!feeding?.allLikes ? '#000000A6' : '#ea9b8e')

    return (
        <>
            <Layout className='login animated fadeIn'>
                <div className='model'>
                    <div className='login-form'>
                        <Row className='mb-3'>
                            <Col span={4} className='pr-2'>
                                <img
                                    src={feeding?.user.profileImage}
                                    alt={feeding?.user.profileImage}
                                    onError={e => (e.target.src = altImageSource)}
                                    className='feeding__user__image'
                                />
                            </Col>
                            <Col span={20} className='font-weight-bold text-capitalize'>
                                {feeding?.user.name}
                            </Col>
                            <Col span={20}>{moment(feeding?.createdAt).format('YYYY.MM.DD')}</Col>
                        </Row>

                        <Row>
                            {feeding && (
                                <AppImagesList
                                    srcFormat={item => API + item.slice(6)}
                                    source={feeding.imgURI}
                                    onClickContainer={() => null}
                                    contained
                                    span={gridHandler(feeding.imgURI.length)}
                                    style={{
                                        width: sizeHandler(feeding.imgURI.length),
                                        height: sizeHandler(feeding.imgURI.length)
                                    }}
                                />
                            )}
                        </Row>

                        <Row>
                            <Col span={24} className='font-weight-bold my-3 px-1'>
                                {feeding?.description}
                            </Col>
                        </Row>
                        <Row>
                            <Col span={22} className='d-flex align-items-center'>
                                <HeartFilled style={{ color: likeColor() }} className='pr-2' />{' '}
                                {feeding?.allLikes || false}
                            </Col>
                        </Row>
                    </div>
                </div>
            </Layout>
        </>
    )
}

export default TapView
