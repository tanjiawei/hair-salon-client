import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, Select, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'
import { storeAnItem, getUser } from '../../../utils/localStorageFunc'

const { Option } = Select

class FromView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            visible: true
        }
        props.onReSetCheckAdmin()
    }

    componentDidMount() {
        storeAnItem('getCheckId', { adminId: getUser().userId, adminName: getUser().username })
    }

    handleClose = () => {
        this.setState({ visible: false })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return
            const values = {
                ...fieldsValue,
                'date-picker': fieldsValue['date-picker'] ? fieldsValue['date-picker'].format('YYYY-MM-DD') : ''
            }
            let { shopName, street1, street2, city, state, postalCode, country, shopTel, userName, password } = values

            const postData = {
                shopName: shopName,
                street1: street1,
                street2: street2 ? street2 : '',
                city: city,
                state: state,
                postalCode: postalCode,
                country: country,
                shopTel: shopTel,
                userName: userName,
                password: password
            }
            axios
                .post(`${API}/admin/addMerchant`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Add Success!')
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    message.error(err.response.data)
                })
        })
    }

    handleConfirmBlur = e => {
        const { value } = e.target
        this.setState({ confirmDirty: this.state.confirmDirty || !!value })
    }

    compareToFirstPassword = (rule, value, callback) => {
        const { form } = this.props
        if (value && value !== form.getFieldValue('password')) {
            callback('Passwords mismatch')
        } else {
            callback()
        }
    }

    validateToNextPassword = (rule, value, callback) => {
        const { form } = this.props
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true })
        }
        callback()
    }

    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Shop', 'Add Shop']}></CustomBreadcrumb>
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label='Shop Name'>
                                    {getFieldDecorator('shopName', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input Shop name!'
                                            }
                                        ]
                                    })(<Input placeholder='Please input Shop name' />)}
                                </Form.Item>
                                <Form.Item label='Street 1'>
                                    {getFieldDecorator('street1', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input Street 1!'
                                            }
                                        ]
                                    })(<Input placeholder='Please input Street 1' />)}
                                </Form.Item>
                                <Form.Item label='Street 2'>
                                    {getFieldDecorator('street2')(<Input placeholder='Please input Street 2' />)}
                                </Form.Item>
                                <Form.Item label='City'>
                                    {getFieldDecorator('city', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input City!'
                                            }
                                        ]
                                    })(<Input placeholder='Please input City' />)}
                                </Form.Item>
                                <Form.Item label='State'>
                                    {getFieldDecorator('state', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input State!'
                                            }
                                        ]
                                    })(
                                        <Select placeholder='Select a state'>
                                            <Option value='AB'>AB</Option>
                                            <Option value='BC'>BC</Option>
                                            <Option value='MB'>MB</Option>
                                            <Option value='NB'>NB</Option>
                                            <Option value='NL'>NL</Option>
                                            <Option value='NS'>NS</Option>
                                            <Option value='NT'>NT</Option>
                                            <Option value='NU'>NU</Option>
                                            <Option value='ON'>ON</Option>
                                            <Option value='PE'>PE</Option>
                                            <Option value='PQ'>PQ</Option>
                                            <Option value='QC'>QC</Option>
                                            <Option value='SK'>SK</Option>
                                            <Option value='YT'>YT</Option>
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label='Postal Code'>
                                    {getFieldDecorator('postalCode', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input Postal Code!'
                                            }
                                        ]
                                    })(<Input placeholder='Please input Postal Code' />)}
                                </Form.Item>
                                <Form.Item label='Country'>
                                    {getFieldDecorator('country', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input Country!'
                                            }
                                        ]
                                    })(
                                        <Select placeholder='Select a country'>
                                            <Option value='Canada'>Canada</Option>
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label='Tel'>
                                    {getFieldDecorator('shopTel', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input tel!'
                                            }
                                        ]
                                    })(<Input placeholder='Please input Tel' />)}
                                </Form.Item>
                                <Form.Item label='User Name'>
                                    {getFieldDecorator('userName', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input user name!'
                                            }
                                        ]
                                    })(<Input placeholder='Please input User Name' />)}
                                </Form.Item>
                                <Form.Item label='Password' hasFeedback>
                                    {getFieldDecorator('password', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input password!'
                                            },
                                            {
                                                validator: this.validateToNextPassword
                                            }
                                        ]
                                    })(<Input.Password placeholder='Please input password' />)}
                                </Form.Item>
                                <Form.Item label='Confirm Password' hasFeedback>
                                    {getFieldDecorator('confirm', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please confirm password!'
                                            },
                                            {
                                                validator: this.compareToFirstPassword
                                            }
                                        ]
                                    })(
                                        <Input.Password
                                            onBlur={this.handleConfirmBlur}
                                            placeholder='Please confirm password'
                                        />
                                    )}
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Add
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(FromView)

export default WrappedNormalLoginForm
