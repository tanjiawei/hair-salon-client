import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Divider, Row, Col, Table, Button, message } from 'antd'
import '@/style/view-style/table.scss'
import axios from '@/api'
import { API } from '@/api/config'
import { storeAnItem } from '../../../utils/localStorageFunc'

class Tables extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
        props.onReSetCheckAdmin()
    }

    editMerchant(text) {
        this.props.history.push({
            pathname: '/merchant/editMerchant',
            search: '?_id=' + text._id,
            state: {
                addressData: text
            }
        })
    }

    checkMerchant(record) {
        const admin = this.state.data.filter(obj => obj._id === record._id)[0]
        storeAnItem('getCheckId', { adminId: admin.id, adminName: admin.userName })
        //do this first and then get the newe getCheckId and send it as a request
        this.props.history.push('/')
    }

    componentDidMount = () => {
        axios
            .get(`${API}/admin/getAllMerchant`)
            .then(res => {
                if (res.status === 200) {
                    var data = []
                    for (let i = 0; i < res.data.length; i++) {
                        data.push({
                            _id: res.data[i]._id,
                            userName: res.data[i].userName,
                            shopName: res.data[i].shopName,
                            street1: res.data[i].street1,
                            street2: res.data[i].street2,
                            city: res.data[i].city,
                            state: res.data[i].state,
                            postalCode: res.data[i].postalCode,
                            country: res.data[i].country,
                            shopTel: res.data[i].shopTel,
                            address:
                                res.data[i].street1 +
                                (res.data[i].street2 !== '' ? ', ' + res.data[i].street2 : '') +
                                ', ' +
                                res.data[i].city +
                                ' ' +
                                res.data[i].state +
                                ', ' +
                                res.data[i].postalCode
                        })
                    }
                    this.setState({ data })
                } else {
                    console.log(res)
                    // 这里处理一些错误信息
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    addButton = () => {
        this.props.history.push({
            pathname: '/merchant/add'
        })
    }

    render() {
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Merchant List']}></CustomBreadcrumb>
                </div>
                <div style={{ float: 'right', paddingBottom: '1rem' }}>
                    <Button style={{}} type='primary' htmlType='submit' onClick={this.addButton}>
                        Add Shop
                    </Button>
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Shop name',
                                        dataIndex: 'shopName',
                                        key: 'shopName',
                                        render: (shopName, record) => {
                                            return (
                                                <Button type='link' onClick={() => this.checkMerchant(record)}>
                                                    {shopName}
                                                </Button>
                                            )
                                        }
                                    },
                                    {
                                        title: 'Address',
                                        dataIndex: 'address',
                                        key: 'address'
                                    },
                                    {
                                        title: 'Action',
                                        key: 'action',
                                        render: (text, record) => (
                                            <span>
                                                <Divider type='vertical' />
                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.editMerchant(text)
                                                    }}>
                                                    Edit
                                                </Button>
                                                <Button type='link'>Delete</Button>
                                            </span>
                                        )
                                    }
                                ]}
                                dataSource={this.state.data}
                            />
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

export default Tables
