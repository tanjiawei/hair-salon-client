import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, Select, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'
import { storeAnItem, getUser } from '../../../utils/localStorageFunc'

const { Option } = Select

class EditMerchantView extends Component {
    constructor(props) {
        super(props)

        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            visible: true,
            _id: this.props.location.state.addressData._id,
            shopName: this.props.location.state.addressData.shopName,
            street1: this.props.location.state.addressData.street1,
            street2: this.props.location.state.addressData.street2,
            city: this.props.location.state.addressData.city,
            state: this.props.location.state.addressData.state,
            postalCode: this.props.location.state.addressData.postalCode,
            country: this.props.location.state.addressData.country,
            shopTel: this.props.location.state.addressData.shopTel,
            userName: this.props.location.state.addressData.userName
        }
    }
    componentDidMount() {
        storeAnItem('getCheckId', { adminId: getUser().userId })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return
            let { shopName, street1, street2, city, state, postalCode, country, shopTel, userName } = fieldsValue

            var postData = {
                _id: this.state._id,
                shopName: shopName,
                street1: street1,
                street2: street2,
                city: city,
                state: state,
                postalCode: postalCode,
                country: country,
                shopTel: shopTel,
                userName: userName
            }
            axios
                .put(`${API}/admin/updateMerchant`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('edit Success!')
                    } else {
                        console.log(res)
                    }
                    console.log(postData)
                })
                .catch(err => {
                    console.log(postData)
                    console.log(err)
                })
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Shop', 'Edit Shop']}></CustomBreadcrumb>
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label='Shop Name'>
                                    {getFieldDecorator('shopName', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input Shop name!'
                                            }
                                        ],
                                        initialValue: `${this.state.shopName}`
                                    })(<Input placeholder='Please input Shop name' />)}
                                </Form.Item>
                                <Form.Item label='Street 1'>
                                    {getFieldDecorator('street1', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input Street 1!'
                                            }
                                        ],
                                        initialValue: `${this.state.street1}`
                                    })(<Input placeholder='Please input Street 1' />)}
                                </Form.Item>
                                <Form.Item label='Street 2'>
                                    {getFieldDecorator('street2', {
                                        initialValue: `${this.state.street2}`
                                    })(<Input placeholder='Please input Street 2' />)}
                                </Form.Item>
                                <Form.Item label='City'>
                                    {getFieldDecorator('city', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input City!'
                                            }
                                        ],
                                        initialValue: `${this.state.city}`
                                    })(<Input placeholder='Please input City' />)}
                                </Form.Item>
                                <Form.Item label='State'>
                                    {getFieldDecorator('state', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input State!'
                                            }
                                        ],
                                        initialValue: `${this.state.state}`
                                    })(
                                        <Select placeholder='Select a state'>
                                            <Option value='AB'>AB</Option>
                                            <Option value='BC'>BC</Option>
                                            <Option value='MB'>MB</Option>
                                            <Option value='NB'>NB</Option>
                                            <Option value='NL'>NL</Option>
                                            <Option value='NS'>NS</Option>
                                            <Option value='NT'>NT</Option>
                                            <Option value='NU'>NU</Option>
                                            <Option value='ON'>ON</Option>
                                            <Option value='PE'>PE</Option>
                                            <Option value='PQ'>PQ</Option>
                                            <Option value='QC'>QC</Option>
                                            <Option value='SK'>SK</Option>
                                            <Option value='YT'>YT</Option>
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label='Postal Code'>
                                    {getFieldDecorator('postalCode', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input Postal Code!'
                                            }
                                        ],
                                        initialValue: `${this.state.postalCode}`
                                    })(<Input placeholder='Please input Postal Code' />)}
                                </Form.Item>
                                <Form.Item label='Country'>
                                    {getFieldDecorator('country', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input Country!'
                                            }
                                        ],
                                        initialValue: `${this.state.country}`
                                    })(
                                        <Select placeholder='Select a country'>
                                            <Option value='Canada'>Canada</Option>
                                        </Select>
                                    )}
                                </Form.Item>
                                <Form.Item label='Tel'>
                                    {getFieldDecorator('shopTel', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input tel!'
                                            }
                                        ],
                                        initialValue: `${this.state.shopTel}`
                                    })(<Input placeholder='Please input Tel' />)}
                                </Form.Item>
                                <Form.Item label='User Name'>
                                    {getFieldDecorator('userName', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input user name!'
                                            }
                                        ],
                                        initialValue: `${this.state.userName}`
                                    })(<Input placeholder='Please input User Name' />)}
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Save
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(EditMerchantView)

export default WrappedNormalLoginForm
