import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, message, Checkbox } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

class EditCustomerView extends Component {
    constructor(props) {
        super(props)

        let customer = JSON.parse(localStorage.getItem('customer'))
        console.log(customer)

        this.state = {
            ...customer
        }
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return

            fieldsValue.newsLetter = fieldsValue.newsLetter ? true : false
            var postData = {
                _id: this.state._id,
                ...fieldsValue
            }

            console.log(postData)

            axios
                .post(`${API}/user/editProfile`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('edit Success!')
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    console.log(err.response)
                })
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Customer Management', 'Edit Customer']} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label={<span>Name</span>}>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please input name' }],
                                        initialValue: `${this.state.name}`
                                    })(<Input placeholder='Please input name' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Email</span>}>
                                    {getFieldDecorator('email', {
                                        rules: [{ required: true, message: 'Please input email' }],
                                        initialValue: `${this.state.email}`
                                    })(<Input placeholder='Please input email' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Phone</span>}>
                                    {getFieldDecorator('phone', {
                                        rules: [{ required: true, message: 'Please input phone' }],
                                        initialValue: `${this.state.phone}`
                                    })(<Input placeholder='Please input phone' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Accept News Letter</span>}>
                                    {getFieldDecorator('newsLetter')(
                                        <Checkbox defaultChecked={this.state.newsLetter}></Checkbox>
                                    )}
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Save
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(EditCustomerView)
export default WrappedNormalLoginForm
