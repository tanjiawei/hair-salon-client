import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Table, Button, message, Input, Modal } from 'antd'
import Highlighter from 'react-highlight-words'
import { SearchOutlined } from '@ant-design/icons'
import '@/style/view-style/table.scss'
import axios from '@/api'
import { API } from '@/api/config'

class CustomerList extends Component {
    state = {
        customerList: [],
        searchText: '',
        searchedColumn: '',
        showAddPoints: false,
        points: ''
    }

    componentDidMount = () => {
        axios
            .get(`${API}/admin/getAllUsers`)
            .then(res => {
                console.log(res.data)
                let data = []
                res.data.forEach(entry => {
                    data.push({
                        _id: entry._id,
                        name: entry.name ? entry.name : 'N/A',
                        email: entry.email ? entry.email : 'N/A',
                        phone: entry.phone ? entry.phone : 'N/A',
                        balance: entry.balance ? entry.balance : 0,
                        points: entry.points ? entry.points : 0,
                        newsLetter: entry.newsLetter
                    })
                })
                this.setState({
                    customerList: data
                })
            })
            .catch(err => {
                message.error(err)
            })
    }

    editCustomer = text => {
        localStorage.setItem('customer', JSON.stringify(text))

        this.props.history.push({
            pathname: '/customer/editCustomer',
            search: '?_id=' + text._id,
            state: {
                ...text
            }
        })
    }

    addButton = () => {
        this.props.history.push({
            pathname: '/customer/addCustomer'
        })
    }

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />

                <>
                    <Button
                        type='primary'
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size='small'
                        style={{ width: 90 }}>
                        Search
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size='small' style={{ width: 90 }}>
                        Reset
                    </Button>
                </>
            </div>
        ),

        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,

        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex]
                      .toString()
                      .toLowerCase()
                      .includes(value.toLowerCase())
                : '',

        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100)
            }
        },

        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            )
    })

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm()
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex
        })
    }

    handleReset = clearFilters => {
        clearFilters()
        this.setState({ searchText: '' })
    }

    handleOk = e => {
        if (this.state.points === '') {
            message.error('Please enter a number')
            return
        }
        console.log(this.selectUserId)

        var postData = {
            _id: this.selectUserId,
            points: this.state.points
        }

        axios
            .post(`${API}/user/addPoints`, postData)
            .then(res => {
                if (res.status === 200) {
                    if (res.data.ok) {
                        message.success('Add Success!')
                        window.location.reload()
                    } else {
                        console.log(res)
                    }
                } else {
                    console.log(res)
                }
                this.setState({ showAddPoints: false, points: '' })
            })
            .catch(err => {
                console.log(err.response)
            })
    }

    handleCancel = e => {
        this.setState({ showAddPoints: false, points: '' })
    }
    onChangeCancelMessage = e => {
        this.setState({ points: e.target.value })
    }

    render() {
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Customer Management', 'Customer List']} />
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Name',
                                        dataIndex: 'name',
                                        key: 'name',
                                        ...this.getColumnSearchProps('name')
                                    },
                                    {
                                        title: 'Email',
                                        dataIndex: 'email',
                                        key: 'email',
                                        ...this.getColumnSearchProps('email')
                                    },
                                    {
                                        title: 'Phone',
                                        dataIndex: 'phone',
                                        key: 'phone',
                                        ...this.getColumnSearchProps('phone')
                                    },
                                    {
                                        title: 'Balance',
                                        dataIndex: 'balance',
                                        key: 'balance'
                                        //...this.getColumnSearchProps('balance')
                                    },
                                    {
                                        title: 'Points',
                                        dataIndex: 'points',
                                        key: 'points'
                                    },
                                    {
                                        title: 'NewsLetter',
                                        dataIndex: 'newsLetter',
                                        key: 'newsLetter',
                                        render: newsLetter => (newsLetter ? 'Yes' : 'No')
                                    },
                                    {
                                        title: 'Action',
                                        key: 'action',
                                        render: (text, record) => (
                                            <span>
                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.editCustomer(text)
                                                    }}>
                                                    Edit
                                                </Button>

                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        // recordSelected = record
                                                        // eventItemSelected = eventItem
                                                        this.selectUserId = text._id

                                                        this.setState({ showAddPoints: true })
                                                    }}>
                                                    Add points
                                                </Button>

                                                <Modal
                                                    title='Add Points'
                                                    visible={this.state.showAddPoints}
                                                    onOk={this.handleOk}
                                                    onCancel={this.handleCancel}>
                                                    <Input
                                                        value={this.state.points}
                                                        onChange={this.onChangeCancelMessage}
                                                        placeholder='Please enter points'
                                                    />
                                                </Modal>
                                                {/* <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.deleteCustomer(text._id)
                                                    }}>
                                                    Delete
                                                </Button> */}
                                            </span>
                                        )
                                    }
                                ]}
                                dataSource={this.state.customerList}
                            />
                        </div>
                    </Col>
                </Row>
                <div style={{ float: 'right' }}>
                    <Button style={{}} type='primary' htmlType='submit' onClick={this.addButton}>
                        Add Customer
                    </Button>
                </div>
            </Layout>
        )
    }
}

export default CustomerList
