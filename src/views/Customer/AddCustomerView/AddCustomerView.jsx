import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, Checkbox, message } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

class AddCustomerView extends Component {
    state = {
        confirmDirty: false
    }

    handleConfirmBlur = e => {
        const { value } = e.target
        this.setState({ confirmDirty: this.state.confirmDirty || !value })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return

            fieldsValue.newsLetter = fieldsValue.newsLetter ? true : false
            let postData = {
                ...fieldsValue,
                // Default password
                password: '12345'
            }

            axios
                .post(`${API}/user/register`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Add Success!')
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    message.error(err.response.data.error)
                })
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Customer Management', 'Add Customer']} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label={<span>Name</span>}>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please input name' }]
                                    })(<Input placeholder='Please input name' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Email</span>}>
                                    {getFieldDecorator('email', {
                                        rules: [{ required: true, message: 'Please input email' }]
                                    })(<Input placeholder='Please input email' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Phone</span>}>
                                    {getFieldDecorator('phone', {
                                        rules: [{ required: true, message: 'Please input phone' }]
                                    })(<Input placeholder='Please input phone' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Accept News Letter</span>}>
                                    {getFieldDecorator('newsLetter')(<Checkbox></Checkbox>)}
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Add
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(AddCustomerView)
export default WrappedNormalLoginForm
