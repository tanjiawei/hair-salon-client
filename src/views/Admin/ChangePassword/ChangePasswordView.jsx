import React, { Component } from 'react'
import { Layout, Row, Col, Form, Button, Input, message, Icon, Divider } from 'antd'
import '@/style/view-style/form.scss'

import axios from '@/api'
import { API } from '@/api/config'
import { storeAnItem } from '@/utils/localStorageFunc.js'
class ChangePassword extends Component {
    state = {
        oldPassword: '',
        newPassword: '',
        newPasswordAgain: ''
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) return
            const { oldPassword, newPassword, newPasswordAgain } = fieldsValue

            const postData = {
                password: oldPassword,
                newPassword,
                newPasswordAgain
            }
            // no need to add header authorization as someone else already add it on apo/index.js on all axios request
            axios
                .post(`${API}/admin/changepassword`, postData)
                .then(({ status, data }) => {
                    if (status === 200) {
                        message.success('Submit Success!')
                        storeAnItem('jwt', data.jwt)
                        this.props.history.goBack()
                    } else message.error('An error appeared, please try again!')
                })
                .catch(() => {
                    message.error('An error appeared, please try again!')
                })
        })
    }

    handleConfirmPassword = (rule, value, callback) => {
        const { getFieldValue } = this.props.form
        if (value && value !== getFieldValue('newPassword')) {
            callback("New passwords don't match !!!")
        }
        callback()
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const { oldPassword, newPassword, newPasswordAgain } = this.state

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <h3 className='pt-3'>Change Your Password</h3>
                            <Divider />
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label={<span>Old Password</span>}>
                                    {getFieldDecorator('oldPassword', {
                                        rules: [{ required: true, message: 'Please input old password.' }],
                                        initialValue: `${oldPassword}`
                                    })(
                                        <Input
                                            prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />}
                                            placeholder='Old Password'
                                            value={''}
                                            type='password'
                                        />
                                    )}
                                </Form.Item>
                                <Form.Item label={<span>New Password</span>}>
                                    {getFieldDecorator('newPassword', {
                                        rules: [{ required: true, message: 'Please input new password.' }],
                                        initialValue: `${newPassword}`
                                    })(
                                        <Input
                                            prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />}
                                            placeholder='New Password'
                                            value={''}
                                            type='password'
                                        />
                                    )}
                                </Form.Item>
                                <Form.Item label={<span>Confirm Password</span>}>
                                    {getFieldDecorator('newPasswordAgain', {
                                        rules: [
                                            {
                                                required: true,
                                                message: 'Please input new password again.'
                                            },
                                            {
                                                validator: this.handleConfirmPassword
                                            }
                                        ],
                                        initialValue: `${newPasswordAgain}`
                                    })(
                                        <Input
                                            prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />}
                                            placeholder='Confirm New Password'
                                            value={''}
                                            type='password'
                                        />
                                    )}
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit' style={{ marginRight: '5rem' }}>
                                        Submit
                                    </Button>
                                    <Button type='secondary' onClick={this.props.history.goBack}>
                                        Go Back
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedChangePasswordForm = Form.create({ name: 'change_password' })(ChangePassword)

export default WrappedChangePasswordForm
