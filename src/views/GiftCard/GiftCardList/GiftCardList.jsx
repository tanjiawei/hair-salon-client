import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Table, Button, message } from 'antd'
import '@/style/view-style/table.scss'
import axios from '@/api'
import { API } from '@/api/config'

class GiftCardList extends Component {
    state = {
        giftCardList: []
    }

    componentDidMount = () => {
        this.fetchGiftCardList()
    }

    fetchGiftCardList = () => {
        axios
            .get(`${API}/giftCard/getAllGiftCard`)
            .then(res => {
                let data = []
                res.data.forEach(entry => {
                    data.push({
                        ...entry
                    })
                })
                this.setState({
                    giftCardList: data
                })
            })
            .catch(err => {
                message.error(err)
            })
    }

    editGiftCard = text => {
        this.props.history.push({
            pathname: '/giftCard/edit',
            search: '?_id=' + text._id,
            state: {
                ...text
            }
        })
    }
    deleteGiftCard = _id => {
        if (!window.confirm('Please confirm the delete operation!')) {
            return
        }
        console.log(_id)
        axios
            .delete(`${API}/giftCard/deleteGiftCard/${_id}`)
            .then(res => {
                if (res.status === 200) {
                    console.log(res)
                    message.success(res.data)
                    this.fetchGiftCardList()
                } else {
                    console.log(res)
                }
            })
            .catch(err => {
                message.error(err)
            })
    }

    addButton = () => {
        this.props.history.push({
            pathname: '/giftCard/add'
        })
    }

    render = () => {
        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Gift Card Management', 'Gift Card List']} />
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <Table
                                columns={[
                                    {
                                        title: 'Name',
                                        dataIndex: 'name',
                                        key: 'name'
                                    },
                                    {
                                        title: 'Value',
                                        dataIndex: 'value',
                                        key: 'value'
                                    },
                                    {
                                        title: 'Price',
                                        dataIndex: 'price',
                                        key: 'price'
                                    },
                                    {
                                        title: 'Amount',
                                        dataIndex: 'amount',
                                        key: 'amount'
                                    },
                                    {
                                        title: 'Redeemed',
                                        dataIndex: 'redeemed',
                                        key: 'redeemed'
                                    },
                                    {
                                        title: 'Note',
                                        dataIndex: 'note',
                                        key: 'note'
                                    },
                                    {
                                        title: 'Action',
                                        key: 'action',
                                        render: (text, record) => (
                                            <span>
                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.editGiftCard(text)
                                                    }}>
                                                    Edit
                                                </Button>

                                                <Button
                                                    type='link'
                                                    onClick={() => {
                                                        this.deleteGiftCard(text._id)
                                                    }}>
                                                    Delete
                                                </Button>
                                            </span>
                                        )
                                    }
                                ]}
                                dataSource={this.state.giftCardList}
                            />
                        </div>
                    </Col>
                </Row>
                <div style={{ float: 'right' }}>
                    <Button style={{}} type='primary' htmlType='submit' onClick={this.addButton}>
                        Add Gift Card
                    </Button>
                </div>
            </Layout>
        )
    }
}

export default GiftCardList
