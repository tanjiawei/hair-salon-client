import React, { Component } from 'react'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import { Layout, Row, Col, Form, Button, Input, message, Icon, Upload, Modal } from 'antd'
import '@/style/view-style/form.scss'
import axios from '@/api'
import { API } from '@/api/config'

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => resolve(reader.result)
        reader.onerror = error => reject(error)
    })
}

class EditGiftCardView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ...this.props.location.state,
            confirmDirty: false,
            previewVisible: false,
            previewImage: '',
            fileList: [],
            isImgUpdated: false
        }
    }

    componentDidMount = () => {
        this.getImgURL()
    }

    getImgURL = () => {
        if (this.state.imgURL) {
            this.setState({
                fileList: [
                    {
                        uid: '-1',
                        name: 'image.jpg',
                        status: 'done',
                        url: `${API}/giftCard/getImg/${this.state.imgURL}`
                    }
                ]
            })
        }
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, fieldsValue) => {
            if (err) {
                console.log(err)
                return
            }

            let postData = new FormData()
            for (let key in fieldsValue) {
                if (key !== 'img') {
                    postData.append(key, fieldsValue[key])
                } else {
                    if (this.state.isImgUpdated && this.state.fileList)
                        postData.append(key, fieldsValue[key].file.originFileObj)
                }
            }

            axios
                .put(`${API}/giftCard/updateGiftCard/${this.state._id}`, postData)
                .then(res => {
                    if (res.status === 200) {
                        message.success('Add Success!')
                    } else {
                        console.log(res)
                    }
                })
                .catch(err => {
                    message.error(err.response.data.error)
                })
        })
    }

    handleConfirmBlur = e => {
        const { value } = e.target
        this.setState({ confirmDirty: this.state.confirmDirty || !value })
    }

    handleCancel = () => this.setState({ previewVisible: false })

    handlePreview = async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj)
        }

        this.setState({
            previewImage: file.url || file.preview,
            previewVisible: true
        })
    }

    handleChange = ({ fileList }) => {
        this.setState({
            fileList,
            isImgUpdated: true
        })
    }

    render = () => {
        const { getFieldDecorator } = this.props.form
        const { previewVisible, previewImage, fileList } = this.state
        const uploadButton = (
            <div>
                <Icon type='plus' />
                <div className='ant-upload-text'>Upload Image</div>
            </div>
        )

        const formItemLayout = {
            labelCol: {
                xs: { span: 16 },
                sm: { span: 6 }
            },
            wrapperCol: {
                xs: { span: 16 },
                sm: { span: 10 }
            }
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 16,
                    offset: 0
                },
                sm: {
                    span: 10,
                    offset: 6
                }
            }
        }

        return (
            <Layout className='animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['Gift Card Management', 'Edit GiftCard']} />
                </div>

                <Row>
                    <Col>
                        <div className='base-style'>
                            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                                <Form.Item label={<span>Name</span>}>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please input name' }],
                                        initialValue: `${this.state.name}`
                                    })(<Input placeholder='Please input name' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Value</span>}>
                                    {getFieldDecorator('value', {
                                        rules: [{ required: true, message: 'Please input value' }],
                                        initialValue: `${this.state.value}`
                                    })(<Input placeholder='Please input value' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Price</span>}>
                                    {getFieldDecorator('price', {
                                        rules: [{ required: true, message: 'Please input price' }],
                                        initialValue: `${this.state.price}`
                                    })(<Input placeholder='Please input price' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Amount</span>}>
                                    {getFieldDecorator('amount', {
                                        rules: [{ required: true, message: 'Please input amount' }],
                                        initialValue: `${this.state.amount}`
                                    })(<Input placeholder='Please input amount' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Note</span>}>
                                    {getFieldDecorator('note', {
                                        initialValue: `${this.state.name}`
                                    })(<Input placeholder='Please input note' value={'fsdfs'} />)}
                                </Form.Item>

                                <Form.Item label={<span>Image</span>}>
                                    {getFieldDecorator('img')(
                                        <Upload
                                            listType='picture-card'
                                            fileList={fileList}
                                            onPreview={this.handlePreview}
                                            onChange={this.handleChange}>
                                            {fileList.length >= 1 ? null : uploadButton}
                                        </Upload>
                                    )}
                                </Form.Item>

                                <Form.Item {...tailFormItemLayout}>
                                    <Button type='primary' htmlType='submit'>
                                        Save
                                    </Button>
                                </Form.Item>
                            </Form>

                            <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                                <img alt='example' style={{ width: '100%' }} src={previewImage} />
                            </Modal>
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(EditGiftCardView)
export default WrappedNormalLoginForm
