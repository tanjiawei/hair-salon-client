import React, { Component } from 'react'
import { Layout, Input, Icon, Form, Button, Divider, message, notification } from 'antd'
import { withRouter } from 'react-router-dom'
import axios from '@/api'
import { API } from '@/api/config'
import { storeAnItem } from '../../utils/localStorageFunc'
import '@/style/view-style/login.scss'

class Login extends Component {
    state = {
        loading: false
    }

    enterLoading = () => {
        this.setState({
            loading: true
        })
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                let { username, password } = values

                var postData = {
                    username: username,
                    password: password
                }
                axios
                    .post(`${API}/admin/login`, postData)
                    .then(res => {
                        if (res.status === 200) {
                            if (res.data.error) {
                                message.error(res.data.error)
                                return
                            }

                            if (res.data.data.superAdmin) {
                                values.auth = 0
                            } else {
                                values.auth = 1
                            }
                            values.userId = res.data.data._id

                            delete values.password

                            storeAnItem('user', values)
                            storeAnItem('jwt', res.data.jwt)
                            storeAnItem('getCheckId', { adminId: values.userId, adminName: values.username })
                            this.enterLoading()
                            message.success('Login Success!')

                            //redirect page based on admin status
                            const adminPath = values.auth === 1 ? '/' : '/merchant/list'
                            this.props.history.push(adminPath)
                        } else {
                            console.log(res)
                        }
                    })
                    .catch(err => {
                        console.log(err)
                    })
            }
        })
    }

    componentDidMount() {
        notification.open({
            message: 'Welcome!',
            duration: null,
            description: (
                <div>
                    <h4>Franchise Owner/who also manage central inventory:</h4>
                    <h6>username: superadmin</h6>
                    <h6>password: 12345</h6>
                    <hr />
                    <h4>Franchisee store 1 manager:</h4>
                    <h6>username: store1</h6>
                    <h6>password: 12345</h6>
                    <hr />
                    <h4>Franchisee store 2 manager:</h4>
                    <h6>username: store2</h6>
                    <h6>password: 12345</h6>
                    <hr />
                    <h6>Note: First time login will be delayed due to free server on sleep mode.</h6>
                </div>
            )
        })
    }

    componentWillUnmount() {
        notification.destroy()
        this.timer && clearTimeout(this.timer)
    }

    render() {
        const { getFieldDecorator } = this.props.form
        return (
            <Layout className='login animated fadeIn'>
                <div className='model'>
                    <div className='login-form'>
                        <h3>BackOffice Management System </h3>
                        <Divider />
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Item>
                                {getFieldDecorator('username', {
                                    rules: [{ required: true, message: 'Enter a valid email address or username.' }]
                                })(
                                    <Input
                                        prefix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        placeholder='Username'
                                    />
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Enter a password.' }]
                                })(
                                    <Input
                                        prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        type='password'
                                        placeholder='Password'
                                    />
                                )}
                            </Form.Item>
                            <Form.Item>
                                <Button
                                    type='primary'
                                    htmlType='submit'
                                    className='login-form-button'
                                    loading={this.state.loading}>
                                    Login
                                </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </Layout>
        )
    }
}

export default withRouter(Form.create()(Login))
