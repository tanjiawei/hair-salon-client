import React, { useState } from 'react'
import AppImage from './AppImage'
import { Divider, Modal, Col } from 'antd'

function AppImagesList({
    title,
    srcFormat,
    source,
    onClickContainer = () => null,
    contained = false,
    span = 3,
    style
}) {
    const [previewVisible, setPreviewVisible] = useState(false)
    const [previewImage, setPreviewImage] = useState('visible')

    const handleCancel = () => setPreviewVisible(false)

    const handlePreview = previewImageUri => {
        setPreviewImage(previewImageUri)
        setPreviewVisible(true)
    }

    return (
        <>
            {title && <Divider orientation='left'>{title}</Divider>}
            <div className='clearfix'>
                {source.length > 0 &&
                    source.map((url, i) => (
                        <Col key={i} span={span}>
                            <AppImage
                                src={srcFormat(url)}
                                onClickContainer={() => onClickContainer(i)}
                                onClickImage={() => handlePreview(srcFormat(url))}
                                contained={contained}
                                span={span}
                                style={style}
                            />
                        </Col>
                    ))}
            </div>
            <Modal visible={previewVisible} footer={null} onCancel={handleCancel}>
                <img alt={previewImage} style={{ width: '100%' }} src={previewImage} />
            </Modal>
        </>
    )
}

export default AppImagesList
