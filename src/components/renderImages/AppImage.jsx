import React from 'react'
import { Badge } from 'antd'
import { CloseCircleFilled } from '@ant-design/icons'
import { altImageSource } from '../../api/config'

function AppImage({ src, onClickContainer, onClickImage, contained, style }) {
    return (
        <Badge
            style={{ margin: 5 }}
            count={<CloseCircleFilled hidden={contained} style={{ color: '#f5222d' }} onClick={onClickContainer} />}>
            <img
                src={src}
                alt={src}
                onClick={onClickImage}
                onError={e => (e.target.src = altImageSource)}
                className='central__merchandise__image--preview'
                style={style}
            />
        </Badge>
    )
}

export default AppImage
