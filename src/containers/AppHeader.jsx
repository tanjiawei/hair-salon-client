import React from 'react'
import PropTypes from 'prop-types'
import { Menu, Dropdown, Icon, Layout, Avatar, Badge } from 'antd'
const { Header } = Layout

const AppHeader = props => {
    let { menuClick, avatar, menuToggle, loginOut, currentUser, currentCheckAdmin } = props
    const center = 'd-flex align-items-center'
    const menu = (
        <Menu>
            <Menu.ItemGroup title='User Setting'>
                <Menu.Divider />
                <Menu.Item className={center}>
                    <Icon type='edit' />
                    <a href='/admin/locationinfo'>Setting</a>
                </Menu.Item>
                <Menu.Item className={center}>
                    <Icon type='safety' />
                    <a href='/admin/changepassword'>Change Password</a>
                </Menu.Item>
            </Menu.ItemGroup>
            <Menu.Divider />
            <Menu.Item className={center}>
                <Icon type='logout' />
                <span onClick={loginOut}>Log Out</span>
            </Menu.Item>
        </Menu>
    )

    return (
        <Header className='header'>
            <div className='left d-flex align-items-center'>
                <Icon
                    style={{ fontSize: '2rem' }}
                    onClick={menuClick}
                    type={menuToggle ? 'menu-unfold' : 'menu-fold'}
                />
                <span style={{ paddingLeft: 20 }}>
                    <b>
                        Hello, {currentUser.username}.{' '}
                        {currentCheckAdmin &&
                            currentUser.username !== currentCheckAdmin.adminName &&
                            `You are visiting ${currentCheckAdmin.adminName}.`}
                    </b>
                </span>
            </div>
            <div className='right'>
                <div className='mr15'>
                    <a rel='noopener noreferrer' href='/'>
                        <Icon type='github' style={{ color: '#000' }} />
                    </a>
                </div>
                <div className='mr15'>
                    <Badge dot={true} offset={[-2, 0]}>
                        <a href='/'>
                            <Icon type='bell' style={{ color: '#000' }} />
                        </a>
                    </Badge>
                </div>
                <div>
                    <Dropdown overlay={menu} overlayStyle={{ width: '20rem' }}>
                        <div className='ant-dropdown-link'>
                            <Avatar icon='user' src={avatar} alt='avatar' style={{ cursor: 'pointer' }} />
                        </div>
                    </Dropdown>
                </div>
            </div>
        </Header>
    )
}

AppHeader.propTypes = {
    menuClick: PropTypes.func,
    avatar: PropTypes.string,
    menuToggle: PropTypes.bool,
    loginOut: PropTypes.func
}

export default React.memo(AppHeader)
