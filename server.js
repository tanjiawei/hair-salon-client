const express = require('express')
const app = express()
const port = process.env.PORT || 5000
const path = require('path')
const fs = require('fs')
const axios = require('./src/API/')
const API = require('./src/API/config')

app.get('/mobile/webview/:id', async function(request, response) {
    const filePath = path.resolve(__dirname, './build', 'index.html')
    // read in the index.html file
    const { data: feeding, status } = await axios.get(`${API}/feedings/${request.params.id}`)
    if (!status || status !== 200) return response.status(500).send('Server error ...')

    fs.readFile(filePath, 'utf8', function(err, data) {
        if (err) {
            return console.log(err)
        }
        // a bit redundant here to fetch again, will improve in the future if time allows
        data = data.replace(/Hair Salon Admin Dashboard/g, `Hair Salon Feed Shared by ${feeding.user.name}`)
        data = data.replace(/\$OG_URL/g, API + '/mobile/webview/' + feeding._id)
        data = data.replace(/\$OG_TITLE/g, `Hair Salon Feed Shared by ${feeding.user.name}`)
        data = data.replace(/\$OG_DESCRIPTION/g, `Hair Salon: ${feeding.description}`)
        result = data.replace(/\$OG_IMAGE/g, API + feeding.imgURI[0].slice(7))

        response.send(result)
    })
})

app.use(express.static(path.resolve(__dirname, './build')))

app.get(/^((?!mobile).)*$/g, function(request, response) {
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function(err, data) {
        if (err) {
            return console.log(err)
        }

        data = data.replace(/\$OG_URL/g, API)
        data = data.replace(/\$OG_TITLE/g, 'Hair Salon')
        result = data.replace(/\$OG_DESCRIPTION/g, 'Hair Salon')
        response.send(result)
    })
})

app.listen(port, () => console.log(`Listening on server port ${port}`))
